package ai.qa.Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DataSetsPage {

	WebDriver driver;
	
	@FindBy(id="actionType")
	private WebElement createDatasetIcon;
	
	@FindBy(id="textfield-TableName")
	private WebElement jiffyTableNameTextField;

	public WebElement getCreateDatasetIcon() {
		return createDatasetIcon;
	}
	
	public WebElement getjiffyTableNameTextField() {
		return jiffyTableNameTextField;
	}
	
	public DataSetsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void navigateToDataSets()
	{
		AppPage appPage = new AppPage(driver);
		appPage.getDatasetsIcon().click();
	}
	
	public List<String> getListOfDataSets()
	{
		List<WebElement> list = driver.findElements(By.xpath("//a[@class='list__dcb_list__fileLink___3A9Kj']"));
		List<String> datasetsList = new ArrayList<String>();
		for(WebElement w : list)
		{
			String text = w.getText();
			datasetsList.add(text);
			
		}
		return datasetsList;
		
	}
	
	public void createJiffyTable(String tableName) throws Throwable
	{
		createDatasetIcon.click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		jiffyTableNameTextField.sendKeys(tableName);
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		driver.findElement(By.xpath("//button[.='Create']")).click();
	}
	
	public void createJiffyTableWithColType(String tableName, String columnName, String columnType) throws Throwable
	{
		createDatasetIcon.click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		jiffyTableNameTextField.sendKeys(tableName);
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Field7")).sendKeys(columnName);
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys(columnType);
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.findElement(By.xpath("//button[.='Create']")).click();
	}
	
	
	public void createJiffyTableWithSelectColType(String tableName, String columnName, String columnType, String option1, String option2, String option3) throws Throwable
	{
		createDatasetIcon.click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		jiffyTableNameTextField.sendKeys(tableName);
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Field7")).sendKeys(columnName);
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys(columnType);
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		//driver.findElement(By.id("textfield-Option1")).sendKeys(option1);
		driver.findElement(By.xpath("//div[@class='mdl-textfield mdl-js-textfield data-table__dcb-datatable__options__optiontext___fq6xQ is-upgraded']/input[1]")).sendKeys(option1);
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi data-table__dcb-datatable__options__addOptions___lTKKk']")).click();
		driver.findElement(By.id("textfield-Option2")).sendKeys(option2);
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi data-table__dcb-datatable__options__addOptions___lTKKk']")).click();
		driver.findElement(By.id("textfield-Option3")).sendKeys(option3);
		driver.findElement(By.xpath("//button[.='Create']")).click();
	}
	
	public void createDatasetWithInlineTable(String tableName, String inlineTableName, String columnName, String columnType) throws Throwable
	{
		createDatasetIcon.click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		jiffyTableNameTextField.sendKeys(tableName);
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Field7")).sendKeys(inlineTableName);
		driver.findElement(By.xpath("//div[@class='select__value-container css-1hwfws3']")).click();
		driver.switchTo().activeElement().sendKeys("Table");
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='Table']")).click();
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-edit___3pRAz data-table__dcb-datatable__column--drop___3VxnE']")).click();
		driver.findElement(By.id("JDI")).click();
		driver.findElement(By.id("textfield-Field2")).sendKeys(columnName);
		driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
		driver.switchTo().activeElement().sendKeys(columnType);
		driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
		driver.findElement(By.xpath("//button[.='save']")).click();
		driver.findElement(By.xpath("//button[.='Create']")).click();
		
	}
	
	public void deleteDataSet(String tableName)
	{
		driver.findElement(By.xpath("//a[.='"+tableName+"']/following::i[2]")).click();
		driver.findElement(By.xpath("//a[.='"+tableName+"']/following::li[2]//span[@data-title='Delete']")).click();
	}
}
