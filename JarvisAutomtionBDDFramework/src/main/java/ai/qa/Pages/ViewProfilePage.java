package ai.qa.Pages;

/**
 * @author Mounika
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ai.qa.GenericUtility.FileUtility;

public class ViewProfilePage {
	
	WebDriver driver;
	FileUtility flib = new FileUtility();
	
	@FindBy(css="span[class='StyledIcon-sc-17a1525-0 ivixVL']")
	private WebElement editButton;
	
	@FindBy(id="firstName")
	private WebElement firstName;
	
	@FindBy(id="lastName")
	private WebElement lastName;
	
	@FindBy(id="organizationName")
	private WebElement organizationName;
	
	@FindBy(id="address")
	private WebElement address;
	
	@FindBy(id="officeAddress")
	private WebElement officeAddress;
	
	@FindBy(id="personalEmail")
	private WebElement personalEmail;
	
	@FindBy(id="personalPhone")
	private WebElement personalPhone;
	
	@FindBy(id="workPhone")
	private WebElement workPhone;
	
	@FindBy(css="div[class='ant-select-selection__rendered']")
	private WebElement timeZone;
	
	@FindBy(xpath="//button[.='SAVE']")
	private WebElement saveButton;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelButton;
	
	//need to add personal photo button, org photo button
	
	@FindBy(xpath="//div[.='Change Password']")
	private WebElement changePasswordTab;
	
	@FindBy(id="currentPassword")
	private WebElement currentPassword;
	
	@FindBy(id="newPassword")
	private WebElement newPassword;
	
	@FindBy(id="confirmNewPassword")
	private WebElement confirmNewPassword;
	
	public ViewProfilePage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public WebElement getEditButton() {
		return editButton;
	}

	public WebElement getFirstName() {
		return firstName;
	}

	public WebElement getLastName() {
		return lastName;
	}

	public WebElement getOrganizationName() {
		return organizationName;
	}

	public WebElement getAddress() {
		return address;
	}

	public WebElement getOfficeAddress() {
		return officeAddress;
	}

	public WebElement getPersonalEmail() {
		return personalEmail;
	}

	public WebElement getPersonalPhone() {
		return personalPhone;
	}

	public WebElement getWorkPhone() {
		return workPhone;
	}

	public WebElement getTimeZone() {
		return timeZone;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getChangePasswordTab() {
		return changePasswordTab;
	}

	public WebElement getCurrentPassword() {
		return currentPassword;
	}

	public WebElement getNewPassword() {
		return newPassword;
	}

	public WebElement getConfirmNewPassword() {
		return confirmNewPassword;
	}

	/**
	 * Used to changePassword
	 * @param newPwd
	 * @param confirmPwd
	 * @throws Throwable
	 */
	public void changePassword(String newPwd) throws Throwable
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getProfileButton().click();
		aPage.getViewProfileButton().click();
		changePasswordTab.click();
		currentPassword.sendKeys(flib.getPropertyKeyValue("password"));
		newPassword.sendKeys(newPwd);
		confirmNewPassword.sendKeys(newPwd);
		saveButton.click();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
