package ai.qa.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UsersPage {
	
	WebDriver driver;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 ixlhbd']")
	private WebElement createUsersIcon;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 DtgpL']")
	private WebElement searchIcon;
	
	@FindBy(id="username.value")
	private WebElement searchByUNField;
	
	@FindBy(id="firstName.value")
	private WebElement searchByFNField;
	
	@FindBy(id="lastName.value")
	private WebElement searchByLNField;
	
	@FindBy(xpath="//label[.='User Type']/..//div[@class='Select__StyledSelect-ssndau-0 eLzNhh ant-select ant-select-enabled']")
	private WebElement searchByUserTypeListBox;
	
	@FindBy(xpath="//label[.='Status']/..//div[@class='Select__StyledSelect-ssndau-0 eLzNhh ant-select ant-select-enabled']")
	private WebElement searchByStatusListBox;
	
	@FindBy(xpath="//label[.='Invite Status']/..//div[@class='Select__StyledSelect-ssndau-0 eLzNhh ant-select ant-select-enabled']")
	private WebElement searchByInviteStatusListBox;
	
	@FindBy(id="emailId.value")
	private WebElement searchByEmailIdField;
	
	@FindBy(xpath="//button[.='Clear']")
	private WebElement clearBtn;
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement submitBtn;
	
	@FindBy(xpath="//button[.='Set As Admin']")
	private WebElement setAsAdminBtn;
	
	@FindBy(xpath="//button[.='Deactivate']")
	private WebElement deactiveBtn;
	
	@FindBy(xpath="//button[.='Archive']")
	private WebElement archiveBtn;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 bqvzII']")
	private WebElement editUserIcon;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 jRYWoa']")
	private WebElement deactiveIcon;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 yMmPm']")
	private WebElement archiveIcon;
	
	@FindBy(xpath="//span[@class='StyledIcon-sc-17a1525-0 bIvlSF']")
	private WebElement nextPageArrowIcon;

	public WebElement getCreateUsersIcon() {
		return createUsersIcon;
	}

	public WebElement getSearchIcon() {
		return searchIcon;
	}

	public WebElement getSearchByUNField() {
		return searchByUNField;
	}

	public WebElement getSearchByFNField() {
		return searchByFNField;
	}

	public WebElement getSearchByLNField() {
		return searchByLNField;
	}

	public WebElement getSearchByUserTypeListBox() {
		return searchByUserTypeListBox;
	}

	public WebElement getSearchByStatusListBox() {
		return searchByStatusListBox;
	}

	public WebElement getSearchByInviteStatusListBox() {
		return searchByInviteStatusListBox;
	}

	public WebElement getSearchByEmailIdField() {
		return searchByEmailIdField;
	}

	public WebElement getClearBtn() {
		return clearBtn;
	}

	public WebElement getSubmitBtn() {
		return submitBtn;
	}

	public WebElement getSetAsAdminBtn() {
		return setAsAdminBtn;
	}

	public WebElement getDeactiveBtn() {
		return deactiveBtn;
	}

	public WebElement getArchiveBtn() {
		return archiveBtn;
	}

	public WebElement getEditUserIcon() {
		return editUserIcon;
	}

	public WebElement getDeactiveIcon() {
		return deactiveIcon;
	}

	public WebElement getArchiveIcon() {
		return archiveIcon;
	}

	public WebElement getNextPageArrowIcon() {
		return nextPageArrowIcon;
	}
	
	public UsersPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * User to search an user by username
	 * @param username
	 */
	public void searchByUsername(String username)
	{ 
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getUsersIcon().click();
		searchIcon.click();
		searchByUNField.sendKeys(username);
		submitBtn.click();	
			try { 
				WebElement expectedUN = driver.findElement(By.xpath("//div[@class='StyledCell-sc-1078hei-0 cKqHjS']/div[.='"+username+"']"));
				boolean v = expectedUN.isDisplayed();
				System.out.println("Username "+username+" is found");
			}	
			catch(Throwable e) {
				 System.out.println("Username "+username+" is not found");
			} 	
	}
	
	/**
	 * Used to search an user by email id
	 * @param email
	 */
	public void searchByEmailId(String email)
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getUsersIcon().click();
		searchIcon.click();
		searchByEmailIdField.sendKeys(email);
		submitBtn.click();
		try { 
			WebElement expectedResult = driver.findElement(By.xpath("//div[@class='StyledCell-sc-1078hei-0 cKqHjS']/div[.='"+email+"']"));
			boolean v = expectedResult.isDisplayed();
			System.out.println("Email Id - "+email+" is found");
		}	
		catch(Throwable e) {
			 System.out.println("Email Id - "+email+" is not found");
		} 		
	}
	

	
	
	
	
}
