package ai.qa.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormsPage {
	
	WebDriver driver;
	
	@FindBy(xpath="//button[.='ADD NEW FORM']")
	private WebElement addNewFormButton;
	
	@FindBy(xpath="//a[.='General Settings']")
	private WebElement generalSettingsTab;
	
	@FindBy(xpath="//a[.='Button Settings']")
	private WebElement buttonSettingsTab;
	
	@FindBy(xpath="//a[.='Field Settings']")
	private WebElement columnSettingsTab;
	
	@FindBy(xpath = "//div[@class='select__value-container select__value-container--has-value css-1hwfws3']")
	private WebElement selectTableDropdown;
	
	@FindBy(xpath="//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe forms__form-item__duplicate___35KqB']")
	private WebElement duplicateFormIcon;
	
	@FindBy(xpath="//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe forms__form-item__delete___1Hd22']")
	private WebElement deleteFormIcon;
	
	@FindBy(id="textfield-FormName")
	private WebElement formNameTextField;
	
	@FindBy(id="selectRoles_0")
	private WebElement selectRolesDropdown;
	
	@FindBy(id="selectStates_0")
	private WebElement selectStatesDropdown;
	
	@FindBy(xpath="//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe forms__editor__add___3siWS forms__editor__add--first___1PJlO']")
	private WebElement addIcon;
	
	@FindBy(xpath="//button[.='CANCEL']")
	private WebElement cancelButton;
	
	@FindBy(xpath="//button[.='SAVE']")
	private WebElement saveButton;
	
	public FormsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public WebElement getAddNewFormButton() {
		return addNewFormButton;
	}

	public WebElement getGeneralSettingsTab() {
		return generalSettingsTab;
	}

	public WebElement getButtonSettingsTab() {
		return buttonSettingsTab;
	}

	public WebElement getColumnSettingsTab() {
		return columnSettingsTab;
	}

	public WebElement getSelectTableDropdown() {
		return selectTableDropdown;
	}

	public WebElement getDuplicateFormIcon() {
		return duplicateFormIcon;
	}

	public WebElement getDeleteFormIcon() {
		return deleteFormIcon;
	}
	
	public WebElement getFormNameTextField() {
		return formNameTextField;
	}

	public WebElement getSelectRolesDropdown() {
		return selectRolesDropdown;
	}

	public WebElement getSelectStatesDropdown() {
		return selectStatesDropdown;
	}

	public WebElement getAddIcon() {
		return addIcon;
	}

	public WebElement getCancelButton() {
		return cancelButton;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}

	public void navigateToForms()
	{
		AppPage appPage = new AppPage(driver);
		appPage.getFormsIcon().click();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
