package ai.qa.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Screen;

import ai.qa.GenericUtility.WebDriverUtility;

public class PresentationsPage {
	WebDriver driver;

	@FindBy(xpath="//button[@class='mdl-button mdl-js-button undefined mdl-button--icon icon-button__dcb-button--icon___Xv2Oe']")
	private WebElement createPresentationIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-control-point___2mz6-']")
	private WebElement addPresentationTypeIcon;
	
	@FindBy(id="placeholder-3mn63")
	private WebElement presentationTitleField;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-done___3y3sB']")
	private WebElement tickIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-save1___3C0QZ']")
	private WebElement publishIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-refresh___3dGYd']")
	private WebElement reloadIcon;
	
	@FindBy(xpath="//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-app-running___3Dg1R']")
	private WebElement replaceDatasetsIcon;
	
	@FindBy(id="papers_0-menu")
	private WebElement moreIconPresentation;
	
	@FindBy(xpath="//li[.='Add Description']")
	private WebElement addDescriptionMenuItem;
	
	@FindBy(xpath="//li[.='Add KPI Ribbon']")
	private WebElement adKPIRibbonMenuItem;
	
	@FindBy(xpath="//li[.='Insert Above']")
	private WebElement insertAboveMenuItem;
	
	@FindBy(xpath="//li[.='Insert Above']")
	private WebElement insertBelowMenuItem;
	
	@FindBy(id="placeholder-22fm9")
	private WebElement cardTitleField;
	
	@FindBy(xpath="//li[@class='button-list__dcb-btn-list__item___1Ar0F']")
	private WebElement moreIconCard;
	
	public WebElement getCreatePresentationIcon() {
		return createPresentationIcon;
	}

	public WebElement getAddPresentationTypeIcon() {
		return addPresentationTypeIcon;
	}

	public WebElement getPresentationTitleField() {
		return presentationTitleField;
	}

	public WebElement getTickIcon() {
		return tickIcon;
	}

	public WebElement getPublishIcon() {
		return publishIcon;
	}

	public WebElement getReloadIcon() {
		return reloadIcon;
	}

	public WebElement getReplaceDatasetsIcon() {
		return replaceDatasetsIcon;
	}

	public WebElement getMoreIconPresentation() {
		return moreIconPresentation;
	}

	public WebElement getAddDescriptionMenuItem() {
		return addDescriptionMenuItem;
	}

	public WebElement getAdKPIRibbonMenuItem() {
		return adKPIRibbonMenuItem;
	}

	public WebElement getInsertAboveMenuItem() {
		return insertAboveMenuItem;
	}

	public WebElement getInsertBelowMenuItem() {
		return insertBelowMenuItem;
	}

	public WebElement getCardTitleField() {
		return cardTitleField;
	}

	public WebElement getMoreIconCard() {
		return moreIconCard;
	}

	public PresentationsPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void navigateToPresentations()
	{
		AppPage appPage = new AppPage(driver);
		appPage.getPresentationsIcon().click();
	}
	
	public void openPresentation(String presentationName)
	{
		navigateToPresentations();
		driver.findElement(By.xpath("//a[.='"+presentationName+"']")).click();
		
	}
	public void createPresentation(String presentationTitle, String tableName) throws Throwable
	{
		//Click on add presentation icon
		createPresentationIcon.click();
		
		//Click on add presentation type
		addPresentationTypeIcon.click();
		
		//Select the type as jiffy table
		driver.findElement(By.xpath("//span[.='Jiffy Table']/../button")).click();
		
		//Wait for jiffy table  type to get selected
		WebDriverUtility wlib = new WebDriverUtility();
		wlib.waitAndClick(driver.findElement(By.xpath("//li[@class='bottom-menu__dcb-quicksheet__btmmenu__item___395EA']")));
		
		//Select the table
		driver.findElement(By.xpath("//ul[@class='folder-navigation__dcb-move__list___11wU8']/li[.='"+tableName+"']")).click();
		
		//Click on plus icon to add all the columns of the table
		WebElement element = driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-control-point___2mz6-']"));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();
		
		//Enter the presentation title
		WebElement title = driver.findElement(By.xpath("//div[@class='header__dcb-header__editor-wrapper___3c802']"));
		title.click();
		Screen s = new Screen();
		s.type(presentationTitle);
		
		//Click on publish icon
		publishIcon.click();
		
		Assert.assertTrue(driver.findElement(By.xpath("//div[.='The changes to the presentation "+presentationTitle+" has been saved']")).isDisplayed());
		wlib.waitForElementInvisibality(driver, driver.findElement(By.xpath("//div[.='The changes to the presentation "+presentationTitle+" has been saved']")));
	}
	
	public void deletePresentation(String presentationTitle)
	{
		driver.findElement(By.xpath("//a[.='"+presentationTitle+"']/following::div[12]")).click();
		driver.findElement(By.xpath("//span[.='Delete']")).click();

	}
}
