package ai.qa.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddUserPage {
	
	WebDriver driver;
		
	@FindBy(xpath="//button[.='Basic Info']")
	private WebElement basicInfoTab;
	
	@FindBy(id="username")
	private WebElement userNameField;
	
	@FindBy(id="firstName")
	private WebElement firstNameField;
	
	@FindBy(id="lastName")
	private WebElement lastNameField;
	
	@FindBy(id="emailId")
	private WebElement emailIdField;
	
	@FindBy(name="canCreateApps")
	private WebElement canCreateAppsToggleBtn;
	
	@FindBy(name="admin")
	private WebElement setAdminToggleBtn;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement cancelBtn;
	
	@FindBy(xpath="//button[.='Save and Continue']")
	private WebElement saveandContinueBtn;
	
	@FindBy(xpath="//button[.='Email Invite']")
	private WebElement emailInviteTab;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement cancelInviteBtn;
	
	@FindBy(xpath="//button[.='Add and Send Invite']")
	private WebElement addandSendInviteBtn;
	
	public WebElement getBasicInfoTab() {
		return basicInfoTab;
	}

	public WebElement getUserNameField() {
		return userNameField;
	}

	public WebElement getFirstNameField() {
		return firstNameField;
	}

	public WebElement getLastNameField() {
		return lastNameField;
	}

	public WebElement getEmailIdField() {
		return emailIdField;
	}

	public WebElement getCanCreateAppsToggleBtn() {
		return canCreateAppsToggleBtn;
	}

	public WebElement getSetAdminToggleBtn() {
		return setAdminToggleBtn;
	}

	public WebElement getCancelBtn() {
		return cancelBtn;
	}

	public WebElement getSaveandContinueBtn() {
		return saveandContinueBtn;
	}

	public WebElement getEmailInviteTab() {
		return emailInviteTab;
	}

	public WebElement getCancelInviteBtn() {
		return cancelInviteBtn;
	}

	public WebElement getAddandSendInviteBtn() {
		return addandSendInviteBtn;
	}

	public AddUserPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Used a create an user using firstname, lastname, emailid
	 */
	public void createUser(String username, String firstname, String lastname, String emailid)
	{
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getUsersIcon().click();
		UsersPage uPage = new UsersPage(driver);
		uPage.getCreateUsersIcon().click();
		userNameField.sendKeys(username);
		firstNameField.sendKeys(firstname);
		lastNameField.sendKeys(lastname);
		emailIdField.sendKeys(emailid);
		saveandContinueBtn.click();
		addandSendInviteBtn.click();
		
		
	}
	
	
	
	
	
}
