package ai.qa.GenericUtility;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.LoginPage;

public class BaseClass {
	
	public ExtentReports reports;
	public ExtentTest feature;
	public ExtentTest scenario;
	public static WebDriver driver;
	public FileUtility flib;
	public ExcelUtility elib;
	public LoginPage lPage;
	public FormsPage fPage;
	public AppSummaryPage aPage;
	
}
