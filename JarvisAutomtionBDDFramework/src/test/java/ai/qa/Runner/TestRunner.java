package ai.qa.Runner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import ai.qa.GenericUtility.WebDriverUtility;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\ai\\qa\\FormsFeatureFile",
				 monochrome=true,
				 dryRun=false,
				 glue = {"ai.qa.FormsStepDefinition","ai.qa.Hook"} ,
				 tags= {"@P1_Smoke"},
				 plugin= {"com.cucumber.listener.ExtentCucumberFormatter:Reports/report.html"}
		)
public class TestRunner {
	/*@BeforeClass
	public static void  deleteReport() throws Throwable
	{
		try 
		{
		if ((new File("C:\\Users\\HP\\git\\JarvisBDD\\JarvisAutomtionBDDFramework\\Reports\\report.html")).delete()) {
		                System.out.println("Pass");     
		            } else {
		                System.out.println("Failed");
		            }

		          } catch (Exception ex) {
		            ex.printStackTrace();
		          }
		
	}*/
	/*@AfterClass
    public static void renameExtentReport() throws Throwable {
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yy-hh.mm.ss");
		Date date=new Date();
		String text = "";
		
		Path source = Paths.get("C:\\Users\\HP\\git\\JarvisBDD\\JarvisAutomtionBDDFramework\\Reports\\report.html");
		try{

		    // rename a file in the same directory
			text = "Report_"+df.format(date)+".html";
		    Files.move(source, source.resolveSibling(text));

		  } catch (IOException e) {
		    e.printStackTrace();
		  }
		
		
		  EmailAttachment attachment = new EmailAttachment();
		  attachment.setPath("C:\\Users\\HP\\git\\JarvisBDD\\JarvisAutomtionBDDFramework\\Reports\\"+text);
		  System.out.println("C:\\Users\\HP\\git\\JarvisBDD\\JarvisAutomtionBDDFramework\\Reports\\"+text);
		  attachment.setDisposition(EmailAttachment.ATTACHMENT);

		  // Create the email message
		  MultiPartEmail email = new MultiPartEmail();
		  email.setHostName("smtp.gmail.com");
		  email.setSmtpPort(465);
		  email.setAuthenticator(new DefaultAuthenticator("authentication mail id", "password"));
		  email.setSSLOnConnect(true);
		  //email.setStartTLSEnabled(true);
		  email.addTo("mounika.p@jiffy.ai", "Mounika");
		  //email.addTo("kavya.pm@jiffy.ai", "Kavya P M");
		  //email.addTo("subin.perumbidy@jiffy.ai", "Subin");
		  email.setFrom("mounika.p@jiffy.ai", "Mounika");
		  email.setSubject("Daily Automation Status Report");
		  email.setMsg("Hi Kavya and Subin"+"\n"+"\n"+"Here is today's report"+"\n"+"\n"+"\n"+"\n"+"Note: Please download the attachment and open with chrome");

		  // add the attachment
		  email.attach(attachment);

		  // send the email
		  email.send();
    }*/
}
