package ai.qa.FormsStepDefinition;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.IConstants;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.DataSetsPage;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddInlineTable_1_StepDefinition extends BaseClass{

	DataSetsPage dPage = new DataSetsPage(driver);
	WebDriverUtility wlib = new WebDriverUtility();
	
	@Given("^User create a Jiffy table with inline table (\\d+)$")
	public void user_create_a_Jiffy_table_with_inline_table(int arg1, DataTable dataTable) throws Throwable {
		dPage.navigateToDataSets();
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> e : data) {
            String tName = e.get("tableName");
            String inline = e.get("inlineTableName");
            String cName = e.get("columnName");
            String cType = e.get("columnType");
	    dPage.createDatasetWithInlineTable(tName, inline, cName, cType);
	}
	}
	
	@When("^Click on Add record link$")
	public void click_on_Add_record_link() throws Throwable {
		//Thread.sleep(1000);
	   driver.findElement(By.xpath("//div[.='Add a record >']")).click();
	}
	
	@When("^Enter the data in inline table \"([^\"]*)\" field as \"([^\"]*)\"$")
	public void enter_the_data_in_inline_table_field_as(String columnName, String text) throws Throwable {
		driver.findElement(By.xpath("//input[@label='"+columnName+"']")).sendKeys(text);
	}

	@Then("^User should be able to enter data \"([^\"]*)\" in inline table \"([^\"]*)\" field$")
	public void user_should_be_able_to_enter_data_in_inline_table_field(String text, String columnName) throws Throwable {
	 	Assert.assertEquals(text, driver.findElement(By.xpath("//input[@label='"+columnName+"']")).getAttribute("value"));
	 	/*JavascriptExecutor js = (JavascriptExecutor) driver;
	 	js.executeScript("document.getElementsByClassName('sc-AxhUy sc-AxheI ezYmGI')");*/
	 	
	 	WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	 	wlib.waitforElement(driver.findElement(By.xpath("//button[.='CANCEL']")));
	 	executor.executeScript("arguments[0].click();", element);
	 	
	}
	
	/*=============================TC93=========================*/
	
	@When("^Enter the data in inline table \"([^\"]*)\" field \"([^\"]*)\"$")
	public void enter_the_data_in_inline_table_field(String columnName, String text) throws Throwable {
		driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).sendKeys(text);
	}

	@Then("^User should be able to enter data \"([^\"]*)\" inline table \"([^\"]*)\" field$")
	public void user_should_be_able_to_enter_data_inline_table_field(String text, String columnName) throws Throwable {
		Assert.assertEquals(text, driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).getAttribute("value"));
		WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	 	wlib.waitforElement(driver.findElement(By.xpath("//button[.='CANCEL']")));
	 	executor.executeScript("arguments[0].click();", element);
	}

	
	/*=============================TC85=========================*/

	@When("^Try to enter the data in \"([^\"]*)\" field of inline table$")
	public void try_to_enter_the_data_in_field_of_inline_table(String columnName) throws Throwable {
		try {
			driver.findElement(By.xpath("//input[@label='"+columnName+"']")).click();
			
	    }
	    catch(Throwable e)
	    {
	    	System.out.println("Not able to enter");
	    }
	}

	@Then("^User should not be able to enter data in \"([^\"]*)\" field of inline table$")
	public void user_should_not_be_able_to_enter_data_in_field_of_inline_table(String arg1) throws Throwable {
		WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	 	wlib.waitforElement(driver.findElement(By.xpath("//button[.='CANCEL']")));
	 	executor.executeScript("arguments[0].click();", element);
		
	}
	
	
	/*=============================TC111=========================*/
	
	@When("^Click on upload link to upload attachment \"([^\"]*)\" in inline table$")
	public void click_on_upload_link_to_upload_attachment_in_inline_table(String link) throws Throwable {
		/*WebElement element = driver.findElement(By.xpath("//div[@class='sc-AxiKw eTFHOF']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("document.getElementBylassNane('sc-AxiKw eTFHOF')");
	 	Thread.sleep(2000);
	 	//executor.executeScript("arguments[0].click();", element);*/
		wlib.handleFileUpload(driver.findElement(By.xpath("//div[@class='sc-AxiKw eTFHOF']")), IConstants.fileInputImagePath, IConstants.openButtonImagePath, link);
	    driver.findElement(By.xpath("//div[.='SAVE']")).click();
	   Thread.sleep(1000);
	}
	
	
	@Then("^User should be able to upload the attachment in inline table$")
	public void user_should_be_able_to_upload_the_attachment_in_inline_table() throws Throwable {
		try 
    	{ 
    	   Assert.assertTrue(driver.findElement(By.xpath("//div[@class='sc-fzoyAV bteyPt']")).isDisplayed());
    	}
    catch(Throwable e)
    {
    	Assert.assertTrue(driver.findElement(By.xpath("//div[@class='sc-AxiKw hXpEhu']")).isDisplayed());
    }
		
		WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	 	
	 	
	}
		/*=============================TC132=========================*/
	
	@When("^Enter the data in \"([^\"]*)\" field as \"([^\"]*)\" in inline table$")
	public void enter_the_data_in_field_as_in_inline_table(String columnName, String text) throws Throwable {
		driver.findElement(By.xpath("//input[@label='"+columnName+"']")).sendKeys(text);
	}

	@Then("^User should be able to enter data \"([^\"]*)\" in \"([^\"]*)\" field in inline table$")
	public void user_should_be_able_to_enter_data_in_field_in_inline_table(String text, String columnName) throws Throwable {
		Assert.assertEquals(text, driver.findElement(By.xpath("//input[@label='"+columnName+"']")).getAttribute("value"));
		WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	 	wlib.waitforElement(driver.findElement(By.xpath("//button[.='CANCEL']")));
	 	executor.executeScript("arguments[0].click();", element);
	}
	
	/*=============================TC100=========================*/
	/*@When("^Enter the data in inline table numeric field as \"([^\"]*)\"$")
	public void enter_the_data_in_inline_table_numeric_field_as(String text) throws Throwable {
		driver.findElement(By.xpath("//input[@placeholder='NumericCol']")).sendKeys(text);
	}

	@Then("^User should be able to enter data \"([^\"]*)\" in inline table numeric field$")
	public void user_should_be_able_to_enter_data_in_inline_table_numeric_field(String text) throws Throwable {
		driver.findElement(By.xpath("//input[@placeholder='NumericCol']")).sendKeys(text); 
		driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	}*/
	
/*===============================TC117&132============================================*/
	
	/*@When("^Click on upload link to upload attachment \"([^\"]*)\"$")
	public void click_on_upload_link_to_upload_attachment(String link) throws Throwable {
	     wlib.handleFileUpload(driver.findElement(By.xpath("//div[.='Upload']")), IConstants.fileInputImagePath, IConstants.openButtonImagePath, link);
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//div[.='SAVE']")).click();
	}

	@Then("^User should be able to upload the attachment$")
	public void user_should_be_able_to_upload_the_attachment() throws Throwable {
	    try 
	    	{ 
	    	   Assert.assertTrue(driver.findElement(By.xpath("//div[.='1 Image']")).isDisplayed());
	    	}
	    catch(Throwable e)
	    {
	    	Assert.assertTrue(driver.findElement(By.xpath("//div[.='1 Attachment']")).isDisplayed());
	    }
	}*/
	
	
	/*===============================TC136============================================*/
	
	@When("^Click on view$")
	public void click_on_view() throws Throwable {
		WebElement element = driver.findElement(By.xpath("//span[.='View']"));
		wlib.waitforElement(element);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].click();", element);
		
	    //driver.findElement(By.xpath("//span[.='View']")).click();
	    
	}
	
	@Then("^Close the record$")
	public void close_the_record() throws Throwable {
		WebElement element = driver.findElement(By.xpath("//i[@class='dx-icon dx-icon-close']"));
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].click();", element);
		
	}
	
	/*===============================TC============================================*/
	
	@Then("^Click on cancel$")
	public void click_on_cancel() throws Throwable {
		WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	}
	
	@Then("^\"([^\"]*)\" column should get displayed in inline table$")
	public void column_should_get_displayed_in_inline_table(String columnName) throws Throwable {
		Assert.assertTrue(driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).isDisplayed());
	    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	}

	@Then("^\"([^\"]*)\" column should not get displayed in inline table$")
	public void column_should_not_get_displayed_in_inline_table(String columnName) throws Throwable {
		try {
			Assert.assertTrue(driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).isDisplayed());	
		}
		catch(Throwable e)
		{
			
		}
	    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	}
}
