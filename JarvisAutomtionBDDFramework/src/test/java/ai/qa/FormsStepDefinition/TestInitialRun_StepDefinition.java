
package ai.qa.FormsStepDefinition;

import java.util.Properties;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.ExcelUtility;
import ai.qa.GenericUtility.JavaUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.SettingsPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestInitialRun_StepDefinition extends BaseClass{

	String categoryName = "";
	@When("^User clicks on app settings icon$")
	public void user_clicks_on_app_settings_icon() throws Throwable {
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getSettingsIcon().click();
	}

	@Then("^User creates the category$")
	public void user_creates_the_category() throws Throwable {
		SettingsPage sPage = new SettingsPage(driver);
		JavaUtility jlib = new JavaUtility();
		ExcelUtility elib = new ExcelUtility();
		//categoryName = "AppCategory "+jlib.getRanDomData();
		//elib.setExcelData("P1 Regression", 1, 0, categoryName);
		sPage.createCategory(categoryName);
		  
   
	}

	@When("^User clicks on App Summary Icon$")
	public void user_clicks_on_App_Summary_Icon() throws Throwable {
		AppSummaryPage aPage = new AppSummaryPage(driver);
		aPage.getAppSummaryIcon().click();
		  
	}

	@Then("^User creates the App$")
	public void user_creates_the_App() throws Throwable {
	ExcelUtility elib = new ExcelUtility();
	AppSummaryPage aPage = new AppSummaryPage(driver);
	aPage.createApp(categoryName, "Automation_Test" , "Automation App");	
	}
	
	//==============================================================================
	@When("^User navigates to datasets page$")
	public void user_navigates_to_datasets_page() throws Throwable {
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.navigateToDataSets();
	}

	@Then("^Creates a jiffyTable$")
	public void creates_a_jiffyTable() throws Throwable {
		DataSetsPage dPage = new DataSetsPage(driver);
		dPage.createJiffyTable(elib.getExcelData("P1 Forms", 5, 2));
	}

	@Then("^Navigate to forms page$")
	public void navigate_to_forms_page() throws Throwable {
		FormsPage fPage = new FormsPage(driver);
		fPage.navigateToForms();  
	}

	@Then("^Select the created table$")
	public void select_the_created_table() throws Throwable {
		FormsPage fPage = new FormsPage(driver);
		fPage.getSelectTableDropdown().click();
		driver.switchTo().activeElement().sendKeys(elib.getExcelData("P1 Forms", 19, 2)); 
	}

	@When("^User navigates to dataset page$")
	public void user_navigates_to_dataset_page() throws Throwable {
	    
	}

	@Then("^Creates a dataset$")
	public void creates_a_dataset() throws Throwable {
	   
	}

	@Then("^Navigates to forms page$")
	public void navigates_to_forms_page() throws Throwable {
	    
	}

	@Then("^user selects the created table$")
	public void user_selects_the_created_table() throws Throwable {
	    
	}

	@Then("^Table should get selected$")
	public void table_should_get_selected() throws Throwable {
	    
	}

	@When("^User navigate to dataset page$")
	public void user_navigate_to_dataset_page() throws Throwable {
	    
	}

	@Then("^user selects the created dataset$")
	public void user_selects_the_created_dataset() throws Throwable {
	    
	}

	@Then("^Created Table should get selected$")
	public void created_Table_should_get_selected() throws Throwable {
	    
	}
	
	
	
	
}
