package ai.qa.FormsStepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.Pages.FormsPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class CustomButton_StepDefinition extends BaseClass{
	
	FormsPage fPage = new FormsPage(driver);
	
	@When("^Clicks on button settings$")
	public void clicks_on_button_settings() throws Throwable {
		fPage.getButtonSettingsTab().click();
		
	}

	@When("^Add a custom button with name \"([^\"]*)\"$")
	public void add_a_custom_button_with_name(String buttonName) throws Throwable {
	    driver.findElement(By.xpath("//button[@class='mdl-button mdl-js-button mdl-button--icon icon-button__dcb-button--icon___Xv2Oe forms__editor__addButtonSetting___2m3HT']")).click();
	    driver.findElement(By.xpath("//div[@class='mdl-textfield mdl-js-textfield mdl-textfield--floating-label forms__editor__buttonName___EgyK- is-upgraded']/input")).sendKeys(buttonName);
	}

	@When("^Select the execution state as \"([^\"]*)\"$")
	public void select_the_execution_state_as(String state) throws Throwable {
		driver.findElement(By.id("selectStates_40")).click();
		   Thread.sleep(1000);
		   WebElement element =  driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']/div[2]"));
		   System.out.println(element.getText());
		   String list[] = element.getText().split("\\n");
			
			for(int i=0; i<list.length;i++)
			{
				if(list[i].equals(state))
				{
					driver.findElement(By.xpath("//div[.='"+state+"']")).click();
					
				}
			} 
	}
	
	@When("^Select the action as \"([^\"]*)\"$")
	public void select_the_action_as(String action) throws Throwable {
	   driver.findElement(By.id("selectAction_40")).click();
	   Thread.sleep(2000);
	   WebElement element =  driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']/div[2]"));
	   System.out.println(element.getText());
	   String list[] = element.getText().split("\\n");
		
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(action))
			{
				driver.findElement(By.xpath("//div[.='"+action+"']")).click();
				
			}
		}
	}

	@When("^Click on \"([^\"]*)\" button$")
	public void click_on_button(String btn) throws Throwable {
	    driver.findElement(By.xpath("//div[.='"+btn+"']")).click();
	}

	@Then("^Execution state will be changed to \"([^\"]*)\"$")
	public void execution_state_will_be_changed_to(String arg1) throws Throwable {
	    Assert.assertTrue(driver.findElement(By.xpath("//td[.='Completed']")).isDisplayed());
	}

	@Then("^Execution state will be saved as \"([^\"]*)\"$")
	public void execution_state_will_be_saved_as(String arg1) throws Throwable {
	    Assert.assertTrue(driver.findElement(By.xpath("//td[.='Inprogress']")).isDisplayed());
	}
	
	@Then("^Form is closed without saving the changes$")
	public void form_is_closed_without_saving_the_changes() throws Throwable {
		try {
			driver.findElement(By.id("form_holder_id")).isDisplayed();
		}
		catch(Throwable e)
		{
			System.out.println("Not displayed");
		}
		
		
	}
	
	@Then("^Form is saved and the window is closed\\.$")
	public void form_is_saved_and_the_window_is_closed() throws Throwable {
		try {
			driver.findElement(By.id("form_holder_id")).isDisplayed();
		}
		catch(Throwable e)
		{
			System.out.println("Not displayed");
		}
		
		Assert.assertTrue(driver.findElement(By.xpath("//td[.='Inprogress']")).isDisplayed());
		
	}
	
}
