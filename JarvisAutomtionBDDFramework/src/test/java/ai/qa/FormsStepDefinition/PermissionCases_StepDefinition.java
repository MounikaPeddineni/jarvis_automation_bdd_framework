package ai.qa.FormsStepDefinition;

import org.openqa.selenium.By;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class PermissionCases_StepDefinition extends BaseClass{

	FormsPage fPage = new FormsPage(driver);
	DataSetsPage dPage = new DataSetsPage(driver);
	
	@When("^Select the role as \"([^\"]*)\"$")
	public void select_the_role_as(String role) throws Throwable {
		if(fPage.getSelectRolesDropdown().getText().equals("4 Options Selected"))
		{
			fPage.getSelectRolesDropdown().click();
			driver.switchTo().activeElement().sendKeys("Designer");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Support");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Business");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Release");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			fPage.getSelectRolesDropdown().click();
			//Thread.sleep(1000);
		}
		fPage.getSelectRolesDropdown().click();
		driver.switchTo().activeElement().sendKeys(role);
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectRolesDropdown().click();
	}

	@When("^User do not select any state$")
	public void user_do_not_select_any_state() throws Throwable {
		if(fPage.getSelectStatesDropdown().getText().equals("4 Options Selected"))
		{
			fPage.getSelectStatesDropdown().click();
			driver.switchTo().activeElement().sendKeys("New Entry");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("New");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Inprogress");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Completed");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			fPage.getSelectStatesDropdown().click();
		}
	}

	@When("^Click on save$")
	public void click_on_save() throws Throwable {
	    fPage.getSaveButton().click();
	}
	
	@Then("^User should not be able to add the record$")
	public void user_should_not_be_able_to_add_the_record() throws Throwable {
		
		try {
			driver.findElement(By.id("form_holder_id")).isDisplayed();
		
		}
		catch(Throwable e)
		{
			
		}
	}
	
	@When("^Select the state as \"([^\"]*)\"$")
	public void select_the_state_as(String state) throws Throwable {
		if(fPage.getSelectStatesDropdown().getText().contains("4 Options Selected"))
		{
			fPage.getSelectStatesDropdown().click();
			driver.switchTo().activeElement().sendKeys("New Entry");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("New");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Inprogress");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Completed");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			fPage.getSelectStatesDropdown().click();
		}
		fPage.getSelectStatesDropdown().click();
		driver.switchTo().activeElement().sendKeys(state);
		driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
		fPage.getSelectStatesDropdown().click();
	}
	
	
	@Then("^User is able to edit record for the given execution state$")
	public void user_is_able_to_edit_record_for_the_given_execution_state() throws Throwable {
		Assert.assertTrue(driver.findElement(By.id("form_holder_id")).isDisplayed());
	}
	
	@Given("^User create a Jiffy table with table name \"([^\"]*)\" and custom execution state \"([^\"]*)\"$")
	public void user_create_a_Jiffy_table_with_table_name_and_custom_execution_state(String tName, String state) throws Throwable {
		dPage.navigateToDataSets();
		dPage.getCreateDatasetIcon().click();
		driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
		dPage.getjiffyTableNameTextField().sendKeys(tName);
		driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
		driver.findElement(By.xpath("//i[@class='_docubefonts__icons___1vjGR _docubefonts__icon-add___1s2Pi data-table__dcb-datatable__options__addOptions___lTKKk']")).click();
		driver.findElement(By.id("textfield-Option4")).sendKeys(state);
		driver.findElement(By.xpath("//button[.='Create']")).click();
	}
	
	@Given("^Select the custom state \"([^\"]*)\"$")
	public void select_the_custom_state(String arg1) throws Throwable {
		if(fPage.getSelectStatesDropdown().getText().contains("Options Selected"))
		{
			fPage.getSelectStatesDropdown().click();
			driver.switchTo().activeElement().sendKeys("New Entry");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("New");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Inprogress");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			driver.switchTo().activeElement().sendKeys("Completed");
			driver.findElement(By.xpath("//div[@class='jarvis-select__jrvs-select___3-ueP undefined css-2b097c-container']//div[contains(@class,'select__menu css-')]")).click();
			fPage.getSelectStatesDropdown().click();
		}
	}
	
	@Then("^User is able to add record$")
	public void user_is_able_to_add_record() throws Throwable {
		Assert.assertTrue(driver.findElement(By.id("form_holder_id")).isDisplayed());
	}
}
