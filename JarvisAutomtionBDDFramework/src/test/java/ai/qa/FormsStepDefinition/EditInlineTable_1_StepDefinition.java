package ai.qa.FormsStepDefinition;

import org.openqa.selenium.By;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.WebDriverUtility;
import cucumber.api.java.en.When;

public class EditInlineTable_1_StepDefinition extends BaseClass{

	WebDriverUtility wlib = new WebDriverUtility();
	
	@When("^Save the data$")
	public void save_the_data() throws Throwable {
	    driver.findElement(By.xpath("//div[.='SAVE']")).click();
	    Thread.sleep(2000);
	    wlib.waitAndClick(driver.findElement(By.xpath("//div[.='SAVE']")));
	    
	}
	
	@When("^Edit the inline table record$")
	public void edit_the_inline_table_record() throws Throwable {
		Thread.sleep(2000);
	    driver.findElement(By.xpath("//span[@class='sc-AxhCb dvUFNe']")).click();
		//wlib.waitAndClick(driver.findElement(By.xpath("//span[@class='sc-AxhCb dvUFNe']")));
	}
}
