package ai.qa.FormsStepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.PresentationsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EditForm_StepDefinition extends BaseClass{
	
	DataSetsPage dPage = new DataSetsPage(driver);
	PresentationsPage pPage = new PresentationsPage(driver);
	WebDriverUtility wlib = new WebDriverUtility();
	
	
	@When("^Save the record$")
	public void save_the_record() throws Throwable {
		//Thread.sleep(1000);
		driver.findElement(By.xpath("//button[.='SAVE']")).click();
	}

	@When("^User clicks on edit record button$")
	public void user_clicks_on_edit_record_button() throws Throwable {
		//driver.findElement(By.xpath("//td[@class='dx-command-edit dx-command-edit-with-icons dx-col-fixed dx-cell-focus-disabled']//a[1]")).click();
		driver.findElement(By.xpath("(//a[@class='dx-link dx-icon-editicon dx-link-icon'])[2]")).click();
	    
	}

	@When("^Edit the text in \"([^\"]*)\" field as \"([^\"]*)\"$")
	public void edit_the_text_in_field_as(String columnName, String text) throws Throwable {

		driver.findElement(By.xpath("//input[@label='"+columnName+"']")).sendKeys(text);
	}

	@Then("^User should be able to edit the \"([^\"]*)\" field as \"([^\"]*)\"$")
	public void user_should_be_able_to_edit_the_field_as(String columnName, String text) throws Throwable {
		Assert.assertEquals(text, driver.findElement(By.xpath("//input[@label='"+columnName+"']")).getAttribute("value"));
		driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	}
	
	
	
	/*=========================TC388=======================*/
	

	@When("^Edit the data in \"([^\"]*)\" field as \"([^\"]*)\"$")
	public void edit_the_data_in_field_as(String columnName, String text) throws Throwable {

		driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).sendKeys(text);
	}
	
	
	/*=========================TC391=======================*/
	
	@When("^Delete the existing image$")
	public void delete_the_existing_image() throws Throwable {
		Thread.sleep(2000);
	    driver.findElement(By.xpath("//span[@class='sc-AxhCb fyChgH']")).click();
	}

}
