package ai.qa.FormsStepDefinition;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.IConstants;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.PresentationsPage;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddForm_StepDefinition extends BaseClass{

	DataSetsPage dPage = new DataSetsPage(driver);
	FormsPage fPage = new FormsPage(driver);
	PresentationsPage pPage = new PresentationsPage(driver);
	WebDriverUtility wlib = new WebDriverUtility();
	
	/*===============================TC19============================================*/
	
	@Given("^User create a Jiffy table with following details$")
	public void user_create_a_Jiffy_table_with_following_details(DataTable dataTable ) throws Throwable {	
		dPage.navigateToDataSets();
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> e : data) {
            String tName = e.get("tableName");
            String cName = e.get("columnName");
            String cType = e.get("columnType");
	    dPage.createJiffyTableWithColType(tName, cName, cType);
	}
        
	}
	@Given("^User creates the presentation$")
	public void user_creates_the_presentation(DataTable presentationData) throws Throwable {
		pPage.navigateToPresentations();
		List<Map<String, String>> data = presentationData.asMaps(String.class, String.class);
        for (Map<String, String> e : data) {
            String pName = e.get("presentationTitle");
            String tName = e.get("tableName");
		driver.navigate().refresh();
		pPage.createPresentation(pName, tName);
	}
	}
	@When("^User clicks on presentation \"([^\"]*)\"$")
	public void user_clicks_on_presentation(String presentationTitle) throws Throwable {
	    
	}

	@When("^Click on plus icon$")
	public void click_on_plus_icon() throws Throwable {
		try {
			driver.findElement(By.xpath("//div[@class='dx-toolbar-items-container']//i[@class='dx-icon dx-icon-addicon']")).getText().isEmpty();
			driver.findElement(By.xpath("//div[@class='dx-toolbar-items-container']//i[@class='dx-icon dx-icon-addicon']")).click();
		}
		catch(Throwable e)
		{
			driver.findElement(By.xpath("//button[.='CANCEL']")).click();
			driver.findElement(By.xpath("//div[@class='dx-toolbar-items-container']//i[@class='dx-icon dx-icon-addicon']")).click();
		}
	}

	@When("^Enter the data in \"([^\"]*)\" field as \"([^\"]*)\"$")
	public void enter_the_data_in_field_as(String columnName, String text) throws Throwable {
		driver.findElement(By.xpath("//input[@label='"+columnName+"']")).sendKeys(text);
		
	}
	
	@Then("^User should be able to enter data \"([^\"]*)\" in \"([^\"]*)\" field$")
	public void user_should_be_able_to_enter_data_in_field(String text, String columnName) throws Throwable {
	 	Assert.assertEquals(text, driver.findElement(By.xpath("//input[@label='"+columnName+"']")).getAttribute("value"));
		driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	
	}

	
	@Then("^User deletes the presentation \"([^\"]*)\"$")
	public void user_deletes_the_presentation(String presentationTitle) throws Throwable {
		pPage.navigateToPresentations();
		pPage.deletePresentation(presentationTitle);
	}

	@Then("^User deletes the table \"([^\"]*)\"$")
	public void user_deletes_the_table(String tableName) throws Throwable {
		dPage.navigateToDataSets();
		driver.navigate().refresh();
		dPage.deleteDataSet(tableName);
	}
	

	/*===============================TC25============================================*/

	
	@When("^Enter the data in \"([^\"]*)\" field \"([^\"]*)\"$")
	public void enter_the_data_in_field(String columnName, String text) throws Throwable {
		driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).sendKeys(text);	
	}
	
	@Then("^User should be able to enter the data in \"([^\"]*)\" field \"([^\"]*)\"$")
	public void user_should_be_able_to_enter_the_data_in_field(String columnName, String text) throws Throwable {
		Assert.assertEquals(text, driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).getAttribute("value"));
		driver.findElement(By.xpath("//button[.='CANCEL']")).click(); 
	}
	
	
	/*===============================TC5============================================*/
	
	@Given("^User create a Jiffy table with table name \"([^\"]*)\"$")
	public void user_create_a_Jiffy_table_with_table_name(String tName) throws Throwable {
		dPage.navigateToDataSets();
		dPage.createJiffyTable(tName);
	}
	
	@When("^Click on plus icon to add record$")
	public void click_on_plus_icon_to_add_record() throws Throwable {
		driver.findElement(By.xpath("//div[@class='dx-item-content dx-toolbar-item-content']//i[@class='dx-icon dx-icon-addicon']")).click();
	}
	
	@When("^Try to enter the data in \"([^\"]*)\" field$")
	public void try_to_enter_the_data_in_field(String columnName) throws Throwable {
		try {
			driver.findElement(By.xpath("//input[@label='"+columnName+"']")).click();
	    }
	    catch(Throwable e)
	    {
	    	System.out.println("Not able to enter");
	    }
	}
	
	@Then("^User should not be able to enter data in \"([^\"]*)\" field$")
	public void user_should_not_be_able_to_enter_data_in_field(String columnName) throws Throwable {
		driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	}
	
	/*===============================TC16============================================*/
	
	@When("^Click on plus icon to add a record$")
	public void click_on_plus_icon_to_add_a_record() throws Throwable {
	    driver.findElement(By.xpath("//i[@class='dx-icon dx-icon-addicon']")).click();
	}
	
	
	@When("^Click on \"([^\"]*)\" field$")
	public void click_on_field(String columnName) throws Throwable {
		//driver.findElement(By.xpath("//div[@class='sc-AxiKw sc-fzoiQi dezErg']")).click();
		driver.findElement(By.xpath("//div[@class='sc-AxiKw sc-fzoant iPLTXm']")).click();
	}

	@When("^Select the state \"([^\"]*)\"$")
	public void select_the_state(String state) throws Throwable {
		WebElement element = driver.findElement(By.xpath("//div[@class='sc-AxiKw sc-fzoant iPLTXm']/div[4]"));
		//WebElement element = driver.findElement(By.xpath("//div[@class='sc-AxiKw sc-fzoiQi dezErg']/div[4]"));
		String list[] = element.getText().split("\\n");
		
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(state))
			{
				driver.findElement(By.xpath("//div[.='"+state+"']")).click();
				
			}
		}
	}

	@Then("^User should be able to select the \"([^\"]*)\" execution state$")
	public void user_should_be_able_to_select_the_execution_state(String state) throws Throwable {
		//Assert.assertEquals(state, driver.findElement(By.xpath("//div[@class='sc-fzqARJ eLpUJW']")).getText());
		Assert.assertEquals(state, driver.findElement(By.xpath("//div[@class='sc-fzomME hhnKZs']")).getText());
		driver.findElement(By.xpath("//button[.='CANCEL']")).click();
	}
	
	
	/*===============================TC43============================================*/
	
	@When("^Click on upload link to upload attachment \"([^\"]*)\"$")
	public void click_on_upload_link_to_upload_attachment(String link) throws Throwable {
	    wlib.handleFileUpload(driver.findElement(By.xpath("//div[.='Upload']")), IConstants.fileInputImagePath, IConstants.openButtonImagePath, link);
	    wlib.waitForElementVisibality(driver, driver.findElement(By.xpath("//div[.='SAVE']")));
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//div[.='SAVE']")).click();
	}

	@Then("^User should be able to upload the attachment$")
	public void user_should_be_able_to_upload_the_attachment() throws Throwable {
	    try 
	    	{ 
	    	Thread.sleep(2000);
	    	   Assert.assertTrue(driver.findElement(By.xpath("//div[.='1  Image']")).isDisplayed());
	    	}
	    catch(Throwable e)
	    {
	    	Assert.assertTrue(driver.findElement(By.xpath("//div[.='1  Attachment']")).isDisplayed());
	    }
	}
	
	
	/*===============================TC69============================================*/
	
	@Then("^Key should be auto generated$")
	public void key_should_be_auto_generated() throws Throwable {
		String text = driver.findElement(By.xpath("//td[@aria-colindex='6']")).getText();
		Assert.assertFalse(text.isEmpty());
	}
	
	/*===============================TC75============================================*/
	@When("^Enter the date in \"([^\"]*)\" field as \"([^\"]*)\"$")
	public void enter_the_date_in_field_as(String columnName, String date) throws Throwable {
	    driver.findElement(By.xpath("//div[.='DateTimeCol']")).click();
	    driver.findElement(By.xpath("//div[.='"+date+"']")).click();
	}

	@Then("^User should be able to save the record$")
	public void user_should_be_able_to_save_the_record() throws Throwable {
	    Assert.assertTrue(driver.findElement(By.xpath("//div[.='The changes made to the form has been saved']")).isDisplayed());
	}
	
	/*===============================TC39============================================*/

	
	@Given("^User create a Jiffy table with select field$")
	public void user_create_a_Jiffy_table_with_select_field(DataTable dataTable) throws Throwable {
		dPage.navigateToDataSets();
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> e : data) {
            String tName = e.get("tableName");
            String cName = e.get("columnName");
            String cType = e.get("columnType");
            String opt1 = e.get("option1");
            String opt2 = e.get("option2");
            String opt3 = e.get("option3");
	    dPage.createJiffyTableWithSelectColType(tName, cName, cType, opt1, opt2, opt3);
        }
	}
	
	@When("^Select the option \"([^\"]*)\"$")
	public void select_the_option(String arg1) throws Throwable {
	    
	}

	@Then("^User should be able to select the \"([^\"]*)\" option$")
	public void user_should_be_able_to_select_the_option(String arg1) throws Throwable {
	   
	}
	
	
	/*===============================TC============================================*/
	
	@When("^clicks on settings icon for \"([^\"]*)\" column$")
	public void clicks_on_settings_icon_for_column(String columnName) throws Throwable {
		fPage.getColumnSettingsTab().click();
		List<WebElement> colsList = driver.findElements(By.xpath("//div[@class='forms__columnRestrictions__columnItem___2IY09']"));
		for(WebElement w : colsList)
		{
			String text = w.getText();
				if(text.equals(columnName)) {
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")));
					driver.findElement(By.xpath("//div[.='"+text+"']/following::button[1]")).click();
					wlib.waitforElement(driver.findElement(By.xpath("//span[.='Allow to View']")));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//span[.='Allow to View']")));
				}
			}
	}

	@When("^Checks whether \"([^\"]*)\" option is checked$")
	public void checks_whether_option_is_checked(String text) throws Throwable {
	    try {
	    	Assert.assertTrue(driver.findElement(By.xpath("//span[.='"+text+"']/..//span[2]")).isSelected());
	    }
	    catch(Throwable e)
	    {
	    	/*List<WebElement> list1 = driver.findElements(By.xpath("//span[@class='mdl-checkbox__label']"));
			for(WebElement element : list1)
			{
			if(element.getText().equals(text))
			{
				element.click();
				fPage.getSaveButton().click();
				wlib.waitForElementInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
			}
			}*/
	    }
	}

		@Then("^\"([^\"]*)\" column should be displayed$")
		public void column_should_be_displayed(String columnName) throws Throwable {
		    Assert.assertTrue(driver.findElement(By.xpath("//input[@label='"+columnName+"']")).isDisplayed());
		    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		}
		
		@When("^Uncheck the \"([^\"]*)\" option$")
		public void uncheck_the_option(String text) throws Throwable {
			try {
		    	Assert.assertFalse(driver.findElement(By.xpath("//span[.='"+text+"']/..//span[2]")).isSelected());
		    	List<WebElement> list1 = driver.findElements(By.xpath("//span[@class='mdl-checkbox__label']"));
				for(WebElement element : list1)
				{
				if(element.getText().equals(text))
				{
					element.click();
					fPage.getSaveButton().click();
					wlib.waitForElementInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
				}
				}
			}
		    catch(Throwable e)
		    {
		    }	
		    
		}
		@Then("^\"([^\"]*)\" column should not be displayed$")
		public void column_should_not_be_displayed(String columnName) throws Throwable {
			
			try {
				Assert.assertTrue(driver.findElement(By.xpath("//input[@label='"+columnName+"']")).isDisplayed());	
			}
			catch(Throwable e)
			{
				
			}
		    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		}
		
		
		@Then("^\"([^\"]*)\" column should get displayed$")
		public void column_should_get_displayed(String columnName) throws Throwable {
			Assert.assertTrue(driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).isDisplayed());
		    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		}
		
		@Then("^\"([^\"]*)\" column should not get displayed$")
		public void column_should_not_get_displayed(String columnName) throws Throwable {
			try {
				Assert.assertTrue(driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).isDisplayed());	
			}
			catch(Throwable e)
			{
				
			}
		    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		}
		
		@Then("^\"([^\"]*)\" column should be displayed for Image cell$")
		public void column_should_be_displayed_for_Image_cell(String columnName) throws Throwable {
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='sc-AxiKw ickDEh']")).isDisplayed());
		    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		}

		@Then("^\"([^\"]*)\" column should not be displayed for Image cell$")
		public void column_should_not_be_displayed_for_Image_cell(String columnName) throws Throwable {
			try {
				Assert.assertTrue(driver.findElement(By.xpath("//div[@class='sc-AxiKw ickDEh']")).isDisplayed());	
			}
			catch(Throwable e)
			{
				
			}
		    driver.findElement(By.xpath("//button[.='CANCEL']")).click();
		}
	    /*================================TC======================*/
		@When("^Check the \"([^\"]*)\" option$")
		public void check_the_option(String option) throws Throwable {
			try {
		    	Assert.assertFalse(driver.findElement(By.xpath("//span[.='"+option+"']/..//span[2]")).isSelected());
		    	List<WebElement> list1 = driver.findElements(By.xpath("//span[@class='mdl-checkbox__label']"));
				for(WebElement element : list1)
				{
				if(element.getText().equals(option))
				{
					element.click();
					fPage.getSaveButton().click();
					wlib.waitForElementInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
				}
				}
			}
		    catch(Throwable e)
		    {
		    }	
		}
		
		@When("^Try to enter in \"([^\"]*)\" field$")
		public void try_to_enter_in_field(String columnName) throws Throwable {
			try {
				driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).click();
		    }
		    catch(Throwable e)
		    {
		    	System.out.println("Not able to enter");
		    }
		}
		
		@When("^Try to upload in \"([^\"]*)\" field$")
		public void try_to_upload_in_field(String arg1) throws Throwable {
		    
			try {
				driver.findElement(By.xpath("//div[.='Upload']")).isDisplayed();
				
			}
			catch(Throwable e)
			{
				System.out.println("Not able to upload");
			}
		}
}
