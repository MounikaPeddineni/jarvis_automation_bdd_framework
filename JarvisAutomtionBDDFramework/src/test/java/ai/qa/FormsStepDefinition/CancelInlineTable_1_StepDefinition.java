package ai.qa.FormsStepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import ai.qa.GenericUtility.BaseClass;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CancelInlineTable_1_StepDefinition extends BaseClass{

	@Then("^Entered data should clear and page should redirect to main table$")
	public void entered_data_should_clear_and_page_should_redirect_to_main_table() throws Throwable {
	    Assert.assertEquals(true, driver.findElement(By.id("formModalId")).isDisplayed());
	}
	
	@Then("^Entered data should not get saved and past data will remain same$")
	public void entered_data_should_not_get_saved_and_past_data_will_remain_same() throws Throwable {
	   Assert.assertEquals(true, driver.findElement(By.xpath("//div[.='DemoText']")).isDisplayed());
	   try {
	   Assert.assertEquals(false, driver.findElement(By.xpath("//div[.='Text']")).isDisplayed());
	   }
	   catch(Throwable e)
	   {
		   System.out.println(true);
	   }
	}
	
	@When("^Cancel the main table$")
	public void cancel_the_main_table() throws Throwable {
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath("//button[.='CANCEL']"));
	 	JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	}

	@Then("^Updated data should not be added$")
	public void updated_data_should_not_be_added() throws Throwable {
		Assert.assertEquals(true, driver.findElement(By.xpath("//div[.='DemoText']")).isDisplayed());
		   try {
		   Assert.assertEquals(false, driver.findElement(By.xpath("//div[.='DemoTextText']")).isDisplayed());
		   }
		   catch(Throwable e)
		   {
			   System.out.println(true);
		   }
	}
}
