package ai.qa.FormsStepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;

import ai.qa.GenericUtility.BaseClass;
import cucumber.api.java.en.Then;

public class CancelForms_StepDefinition extends BaseClass{

	@Then("^No data should get saved in main table$")
	public void no_data_should_get_saved_in_main_table() throws Throwable {
	    Assert.assertEquals(true,driver.findElement(By.xpath("//span[.='No data']")).isDisplayed());
	}
}
