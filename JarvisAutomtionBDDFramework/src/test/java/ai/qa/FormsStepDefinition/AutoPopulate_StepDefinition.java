package ai.qa.FormsStepDefinition;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.DataSetsPage;
import ai.qa.Pages.FormsPage;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AutoPopulate_StepDefinition extends BaseClass{

	FormsPage fPage = new FormsPage(driver);
	DataSetsPage dPage = new DataSetsPage(driver);
	WebDriverUtility wlib = new WebDriverUtility();
	
	@Given("^User create a Jiffy table with multiple columns  for following details$")
	public void user_create_a_Jiffy_table_with_multiple_columns_for_following_details(DataTable dataTable) throws Throwable {
			dPage.navigateToDataSets();
			List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
			for (Map<String, String> e : data) {
            String tName = e.get("tableName");
            String cName1 = e.get("columnName1");
            String cType1 = e.get("columnType1");
            String cName2 = e.get("columnName2");
            String cType2 = e.get("columnType2");
            String cName3 = e.get("columnName3");
            String cType3 = e.get("columnType3");
			dPage.getCreateDatasetIcon().click();
			driver.findElement(By.xpath("//span[@data-title='Jiffy Table']")).click();
			dPage.getjiffyTableNameTextField().sendKeys(tName);
			driver.findElement(By.xpath("//div[@class='data-table__dcb-datatable__table___2Ggwt']//div[2]//div[@class='select__single-value css-1uccc91-singleValue']")).click();
			driver.findElement(By.id("JDI")).click();
			driver.findElement(By.id("textfield-Field7")).sendKeys(cName1);
			driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
			driver.switchTo().activeElement().sendKeys(cType1);
			driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
			
			driver.findElement(By.id("JDI")).click();
			driver.findElement(By.id("textfield-Field8")).sendKeys(cName2);
			driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
			driver.switchTo().activeElement().sendKeys(cType2);
			driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
			
			driver.findElement(By.id("JDI")).click();
			driver.findElement(By.id("textfield-Field9")).sendKeys(cName3);
			driver.findElement(By.xpath("//div[@class='select__placeholder css-1wa3eu0-placeholder']")).click();
			driver.switchTo().activeElement().sendKeys(cType3);
			driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
			driver.findElement(By.xpath("//button[.='Create']")).click();
			
			}
		
	}
	
	@Given("^User click on save button$")
	public void user_click_on_save_button() throws Throwable {
	
		driver.findElement(By.xpath("//button[.='SAVE']")).click();
		Thread.sleep(2000);
		
	}

	@When("^User navigate to forms and select the table \"([^\"]*)\"$")
	public void user_navigate_to_forms_and_select_the_table(String tableName) throws Throwable {
		fPage.navigateToForms();
		driver.navigate().refresh();
		if(fPage.getSelectTableDropdown().getText().equals(tableName))
		{
		
		}
		else
		{
			fPage.getSelectTableDropdown().click();
			driver.switchTo().activeElement().sendKeys(tableName);
			WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
			String list[] = element.getText().split(" ");
			for(int i=0; i<list.length;i++)
			{
				if(list[i].equals(tableName))
				{
					driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+tableName+"']")).click();
				}
			}
		}
		
		Assert.assertEquals(tableName, fPage.getSelectTableDropdown().getText());
	}

	@When("^User go to the field settings tab$")
	public void user_go_to_the_field_settings_tab() throws Throwable {
		fPage.getColumnSettingsTab().click();
	}

	@When("^clicks on auto population settings icon for \"([^\"]*)\" column$")
	public void clicks_on_auto_population_settings_icon_for_column(String columnName) throws Throwable {
		driver.findElement(By.xpath("//div[.='"+columnName+"']/..//i[2]")).click();
	}

	@When("^Adds the auto population with the following details$")
	public void adds_the_auto_population_with_the_following_details(DataTable dataTable) throws Throwable {
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> e : data) {
            String lookUp = e.get("LookUpTable");
            String fieldName = e.get("FieldName");
            String orderByField = e.get("OrderByField");
            String orderBYType = e.get("OrderByType");
            String field = e.get("Field");
            String operator = e.get("Operator");
            String constant = e.get("Constant");
		driver.findElement(By.xpath("//div[.='Select lookup table']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(lookUp);
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(lookUp))
			{          
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+lookUp+"']")).click();
			}
		}
	
		driver.findElement(By.xpath("//div[.='Key field of the lookup table']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(fieldName);
		WebElement element1 = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list1[] = element1.getText().split(" ");
		for(int i=0; i<list1.length;i++)
		{
			if(list1[i].equals(fieldName))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+fieldName+"']")).click();
			}
		}
		
		
		
		driver.findElement(By.xpath("//div[.='Order by Field']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(orderByField);
		WebElement element2 = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list2[] = element2.getText().split(" ");
		for(int i=0; i<list2.length;i++)
		{
			if(list2[i].equals(orderByField))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+orderByField+"']")).click();
			}
		}
		
		driver.findElement(By.xpath("//div[.='Order by Type']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(orderBYType);
		WebElement element3 = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list3[] = element3.getText().split(" ");
		for(int i=0; i<list3.length;i++)
		{
			if(list3[i].equals(orderBYType))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+orderBYType+"']")).click();
			}
		}
		
		Thread.sleep(1000);
		//driver.findElement(By.xpath("//div[@class='forms__autoPopulation__action___5uj0q']//button[.='SAVE']")).click();
        }
	}

	
	@When("^Adds the auto population with the condition$")
	public void adds_the_auto_population_with_the_condition(DataTable dataTable) throws Throwable {
	  List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> e : data) {
            String lookUp = e.get("LookUpTable");
            String fieldName = e.get("FieldName");
            String orderByField = e.get("OrderByField");
            String orderBYType = e.get("OrderByType");
            String field = e.get("Field");
            String operator = e.get("Operator");
            String constant = e.get("Constant");
		driver.findElement(By.xpath("//div[.='Select lookup table']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(lookUp);
		WebElement element = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list[] = element.getText().split(" ");
		for(int i=0; i<list.length;i++)
		{
			if(list[i].equals(lookUp))
			{          
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+lookUp+"']")).click();
			}
		}
	
		driver.findElement(By.xpath("//div[.='Key field of the lookup table']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(fieldName);
		WebElement element1 = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list1[] = element1.getText().split(" ");
		for(int i=0; i<list1.length;i++)
		{
			if(list1[i].equals(fieldName))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+fieldName+"']")).click();
			}
		}
		
		driver.findElement(By.xpath("//span[.='  Add Conditions']")).click();
		driver.findElement(By.xpath("//div[.='Field']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(field);
		WebElement element4 = driver. findElement(By.cssSelector("div[class='select__menu css-vju969-menu']"));
		String list4[] = element4.getText().split(" ");
		for(int i=0; i<list4.length;i++)
		{
			if(list4[i].equals(field))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
				//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+field+"']")).click();
			}
		}
		
		driver.findElement(By.xpath("//div[.='Operator']/..//div[@class='select__value-container css-1hwfws3']")).click();
		driver.switchTo().activeElement().sendKeys(operator);
		Thread.sleep(1000);
		WebElement element5 = driver.findElement(By.xpath("//div[.='Operator']/..//div[@class='jarvis-select__jrvs-select___3-ueP forms__select__select___13ZWr css-2b097c-container']/div[2]"));
		String text = element5.getText();
		System.out.println(element5.getText());
		driver.findElement(By.xpath("//div[.='"+text+"']")).click();
		/*String list5[] = element5.getText().split("\n");
		for(int i=0; i<list5.length;i++)
		{
				if(list5[i].equals(operator))
			{
					//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]")).click();
					//driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='+operator+']")).click();
				//driver.findElement(By.xpath("//div[.='Operator']/..//div[@class='jarvis-select__jrvs-select___3-ueP forms__select__select___13ZWr css-2b097c-container']//div[@class='select__menu css-gc3r0o-menu']")).click();
				//  driver.findElement(By.xpath("//div[contains(@class,'select__menu css-gc3r0o-menu')]")).click();
			}
		}*/
		Thread.sleep(1000);
		driver.findElement(By.id("textfield-Constant")).sendKeys(constant);
		
		
		driver.findElement(By.xpath("//div[.='Order by Field']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(orderByField);
		WebElement element2 = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list2[] = element2.getText().split(" ");
		for(int i=0; i<list2.length;i++)
		{
			if(list2[i].equals(orderByField))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+orderByField+"']")).click();
			}
		}
		
		driver.findElement(By.xpath("//div[.='Order by Type']/..//div[@class='select__value-container css-1hwfws3']")).click();
	    driver.switchTo().activeElement().sendKeys(orderBYType);
		WebElement element3 = driver. findElement(By.cssSelector("div[class='select__menu css-tjxlve-menu']"));
		String list3[] = element3.getText().split(" ");
		for(int i=0; i<list3.length;i++)
		{
			if(list3[i].equals(orderBYType))
			{
				driver.findElement(By.xpath("//div[contains(@class,'select__menu css-')]//div[.='"+orderBYType+"']")).click();
			}
		}
		
		Thread.sleep(1000);
        }
	}
	
	@Then("^Quantity greater than or equal \"([^\"]*)\" should be displayed$")
	public void quantity_greater_than_or_equal_should_be_displayed(String arg1) throws Throwable {
	 	//Thread.sleep(2000);
	    WebElement element = driver.findElement(By.xpath("//input[@class='sc-fzoiQi JSJPN']/../div[3]"));
	    String list1[] = element.getText().split("\n");
	    for(int i=0; i<list1.length;i++)
		{
	    	int j=Integer.parseInt(list1[i]);
	    	Assert.assertTrue(j>=3);
			
		}
	    
	}
	
	

	/*@When("^Adds the condition$")
	public void adds_the_condition(DataTable arg1) throws Throwable {
	    //driver.findElement(By.xpath("//span[.='  Add Conditions']")).click();
	}

	@When("^Clicks on \"([^\"]*)\" column$")
	public void clicks_on_column(String arg1) throws Throwable {
		//driver.findElement(By.xpath("//input[@placeholder='"+column+"']")).click();
	    driver.findElement(By.id("select_0DepartureFrom")).click();
	}

	@Then("^The names having number \"([^\"]*)\" should be displayed$")
	public void the_names_having_number_should_be_displayed(String arg1) throws Throwable {
		WebElement element3 = driver. findElement(By.xpath("//div[@class='select__menu css-dh29bt-menu']"));
		String list3[] = element3.getText().split("\n");
		System.out.println(element3);
	}*/
	
	
	/*========================TC=========================*/
	@When("^Clicks on \"([^\"]*)\" column$")
	public void clicks_on_column(String columnName) throws Throwable {
		driver.findElement(By.xpath("//div[@class='sc-AxiKw sc-fznJRM bEYrhy']")).click();
	    //driver.findElement(By.xpath("//div[.='"+columnName+"']")).click();
	}

	@When("^Select the country as \"([^\"]*)\"$")
	public void select_the_country_as(String option) throws Throwable {
		//Thread.sleep(2000);
		
		driver.findElement(By.xpath("//div[.='"+option+"']")).click();
	}

	@Then("^User should be able to select the \"([^\"]*)\"$")
	public void user_should_be_able_to_select_the(String option) throws Throwable {
	    Assert.assertEquals(true, driver.findElement(By.xpath("//input[@value='"+option+"']")).isDisplayed());
	}
	
	@When("^Check the \"([^\"]*)\" checkbox$")
	public void check_the_checkbox(String option) throws Throwable {

		WebElement element = driver.findElement(By.xpath("//span[.='"+option+"']/../span[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
		// driver.findElement(By.xpath("//span[.='"+option+"']/../span[2]")).click();
	}
	
	@When("^Save the autopopulate$")
	public void save_the_autopopulate() throws Throwable {
		driver.findElement(By.xpath("//div[@class='forms__autoPopulation__action___5uj0q']//button[.='SAVE']")).click();
	}
	
	@When("^Enter the data as \"([^\"]*)\"$")
	public void enter_the_data_as(String text) throws Throwable {
		driver.findElement(By.xpath("//div[@class='sc-AxiKw sc-fznJRM bEYrhy']")).click();
		driver.switchTo().activeElement().sendKeys(text);
		
	}
	
	@Then("^Refresh the page$")
	public void refresh_the_page() throws Throwable {
	    driver.navigate().refresh();
	}
	
	@Then("^\"([^\"]*)\" is selected by default$")
	public void is_selected_by_default(String option) throws Throwable {
		Assert.assertEquals(true, driver.findElement(By.xpath("//input[@value='"+option+"']")).isDisplayed());
	}
}
