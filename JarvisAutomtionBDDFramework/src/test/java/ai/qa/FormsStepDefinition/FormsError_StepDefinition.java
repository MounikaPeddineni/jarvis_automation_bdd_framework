package ai.qa.FormsStepDefinition;

import java.awt.Robot;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.sun.glass.events.KeyEvent;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.WebDriverUtility;
import ai.qa.Pages.FormsPage;
import ai.qa.Pages.PresentationsPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class FormsError_StepDefinition extends BaseClass{

	WebDriverUtility wlib = new WebDriverUtility();
	FormsPage fPage = new FormsPage(driver);
	PresentationsPage pPage = new PresentationsPage(driver);
	
	@When("^Check \"([^\"]*)\" option$")
	public void check_option(String text) throws Throwable {
		List<WebElement> list1 = driver.findElements(By.xpath("//span[@class='mdl-checkbox__label']"));
		for(WebElement element : list1)
		{
		if(element.getText().equals(text))
		{
			element.click();
			fPage.getSaveButton().click();
			wlib.waitForElementInvisibality(driver, driver.findElement(By.xpath("//div[.='Your changes have been saved']")));
		}
		}
		
		
	}
	
	@When("^Clear the existing text in \"([^\"]*)\"$")
	public void clear_the_existing_text_in(String columnName) throws Throwable {
		driver.findElement(By.xpath("//input[@label='"+columnName+"']")).click();
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
	       rb.keyPress(KeyEvent.VK_A);
	       rb.keyRelease(KeyEvent.VK_A);
	       rb.keyRelease(KeyEvent.VK_CONTROL);
	       rb.keyPress(KeyEvent.VK_BACKSPACE);
	       rb.keyRelease(KeyEvent.VK_BACKSPACE);
		
			
	}
	
	@Then("^Error message should be displayed as \"([^\"]*)\" for \"([^\"]*)\"$")
	public void error_message_should_be_displayed_as_for(String msg, String columnName) throws Throwable {
		driver.findElement(By.xpath("//button[.='SAVE']")).click();
		try {
	    	driver.findElement(By.xpath("//div[.='The changes made to the form has been saved'")).isDisplayed();
	    }
	    catch(Throwable e)
	    {
	    	driver.findElement(By.xpath("//input[@label='"+columnName+"']")).click();
	    	Assert.assertEquals(true, driver.findElement(By.xpath("//div[.='Field is mandatory']")).isDisplayed());
	    }
	    
	}
	
	@When("^User navigates to presentation \"([^\"]*)\"$")
	public void user_navigates_to_presentation(String ppt) throws Throwable {
	    pPage.openPresentation(ppt);
	}
	
	/*====================TC====================*/
	
	@When("^Click on plus icon to add a column$")
	public void click_on_plus_icon_to_add_a_column() throws Throwable {
	   driver.findElement(By.id("Add_0")).click(); 
	}

	@When("^Click on Add Sections$")
	public void click_on_Add_Sections() throws Throwable {
	    driver.findElement(By.xpath("//li[.='Add Sections']")).click();
	}

	@When("^Enter the section name as \"([^\"]*)\"$")
	public void enter_the_section_name_as(String section) throws Throwable {
	    driver.findElement(By.id("textfield-EnterSectionName")).sendKeys(section);;
	}

	@When("^Select the field as \"([^\"]*)\"$")
	public void select_the_field_as(String columnName) throws Throwable {
		WebElement element = driver.findElement(By.xpath("//div[.='"+columnName+"']/input"));
	   JavascriptExecutor executor = (JavascriptExecutor)driver;
	   executor.executeScript("arguments[0].click();", element);
	}

	@When("^Click on create$")
	public void click_on_create() throws Throwable {
	    driver.findElement(By.xpath("//button[.='CREATE']")).click();
	}
	
	/*====================TC====================*/
	
	@Then("^Error message should get displayed as \"([^\"]*)\" for \"([^\"]*)\"$")
	public void error_message_should_get_displayed_as_for(String msg, String columnName) throws Throwable {
		
		try {
	    	driver.findElement(By.xpath("//div[.='The changes made to the form has been saved'")).isDisplayed();
	    }
	    catch(Throwable e)
	    {
	    	driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).click();
	    	Assert.assertEquals(true, driver.findElement(By.xpath("//div[.='"+msg+"']")).isDisplayed());
	    }
		
		
	}
	@When("^Clear the text in \"([^\"]*)\"$")
	public void clear_the_text_in(String columnName) throws Throwable {
	 	driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).click();
		Robot rb = new Robot();
		   rb.keyPress(KeyEvent.VK_CONTROL);
	       rb.keyPress(KeyEvent.VK_A);
	       rb.keyRelease(KeyEvent.VK_A);
	       rb.keyRelease(KeyEvent.VK_CONTROL);
	       rb.keyPress(KeyEvent.VK_BACKSPACE);
	       rb.keyRelease(KeyEvent.VK_BACKSPACE);
		
		
	}
	
	@When("^Click on error icon$")
	public void click_on_error_icon() throws Throwable {
	  driver.findElement(By.xpath("//span[@class='sc-AxhCb hytVrx sc-fznxsB ixMaUZ']")).click();  
	}
	
	@Then("^Error message should be displayed as \"([^\"]*)\"$")
	public void error_message_should_be_displayed_as(String msg) throws Throwable {
		try {
	    	driver.findElement(By.xpath("//div[.='The changes made to the form has been saved'")).isDisplayed();
	    }
	    catch(Throwable e)
	    {
	    	//driver.findElement(By.xpath("//textarea[@label='"+columnName+"']")).click();
	    	Assert.assertEquals(true, driver.findElement(By.xpath("//div[.='"+msg+"']")).isDisplayed());
	    }
	}
}
