package ai.qa.FormsStepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.Pages.TaskPage;
import cucumber.api.java.en.Given;

public class Validation_StepDefinition extends BaseClass{

	TaskPage tPage = new TaskPage(driver);
	@Given("^User creates a task  \"([^\"]*)\" with function node$")
	public void user_creates_a_task_with_function_node(String taskName) throws Throwable {
	    tPage.createTask(taskName);
	}

	@Given("^User adds a user defined function in task \"([^\"]*)\"$")
	public void user_adds_a_user_defined_function_in_task(String taskName) throws Throwable {
		driver.navigate().refresh();
		tPage.openTask(taskName);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[.='EDIT']")).click();
		driver.findElement(By.xpath("//i[@class='icon-panel-exp iconmoon node_panel_arrow']")).click();
		driver.findElement(By.xpath("//input[@class='form-control node_search_input']")).sendKeys("function");
		//WebElement src = driver.findElement(By.id("v-470"));
		//src.click();
		//WebElement tar = driver.findElement(By.id("play_ground"));
		
		String javascript = "document.getElementById('v-478')";
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;  
		WebElement src = (WebElement) jsExecutor.executeScript(javascript);
		Thread.sleep(1000);
		String js = "document.getElementsByClassName('viewport')";
		WebElement tar = (WebElement) jsExecutor.executeScript(js);
		
		Actions act = new Actions(driver);
		act.dragAndDrop(src, tar);
		
		
	}	
	
}
