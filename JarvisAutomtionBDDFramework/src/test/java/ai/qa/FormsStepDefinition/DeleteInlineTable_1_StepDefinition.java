package ai.qa.FormsStepDefinition;

import org.openqa.selenium.By;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.WebDriverUtility;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeleteInlineTable_1_StepDefinition extends BaseClass{
	
	WebDriverUtility wlib = new WebDriverUtility();
	
	@When("^Click on delete record icon$")
	public void click_on_delete_record_icon() throws Throwable {
		wlib.waitAndClick(driver.findElement(By.xpath("//span[@class='sc-AxhCb jIKnpt']")));
	}

	@Then("^User should be able to delete record using 'DELETE RECORD' icon$")
	public void user_should_be_able_to_delete_record_using_DELETE_RECORD_icon() throws Throwable {
	    try {
		driver.findElement(By.xpath("//div[.='DemoText']")).isDisplayed();
	    }
	    catch(Throwable e)
	    {
	    	
	    }
	}

	
}
