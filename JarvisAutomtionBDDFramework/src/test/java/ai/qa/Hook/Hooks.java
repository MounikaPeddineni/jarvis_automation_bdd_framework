package ai.qa.Hook;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import ai.qa.GenericUtility.BaseClass;
import ai.qa.GenericUtility.FileUtility;
import ai.qa.Pages.AppSummaryPage;
import ai.qa.Pages.LoginPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class Hooks  extends BaseClass {

	@Before
	public void beforeScenario() throws Throwable
	{
		   flib = new FileUtility();
		   driver = new ChromeDriver();
		   driver.manage().window().maximize();
		   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		   driver.get(flib.getPropertyKeyValue("url"));
		   lPage = new LoginPage(driver);
		   lPage.loginToApp(flib.getPropertyKeyValue("username"), flib.getPropertyKeyValue("password"));
		   aPage = new AppSummaryPage(driver);
		   aPage.openApp(flib.getPropertyKeyValue("appcategory"), flib.getPropertyKeyValue("appname"));
	}
	
	@After
	public void afterScenario() throws Throwable
	{
		aPage = new AppSummaryPage(driver);
		//aPage.logout();
		driver.close();
		//driver.navigate().refresh();
		
	}
	
	
}
