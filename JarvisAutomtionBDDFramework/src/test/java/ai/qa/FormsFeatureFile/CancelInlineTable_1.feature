@P1_Smoke
Feature: Cancel Inline Table 1

Scenario: TC361 - To verify when user clicks cancel after entering all the data in inline table 1
Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC361_Table | TC361_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC361_PPT | TC361_Table | 		
	When User clicks on presentation "TC361_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	And Click on cancel
	Then Entered data should clear and page should redirect to main table
	And Click on cancel
	And User deletes the presentation "TC361_PPT"
	And User deletes the table "TC361_Table"
	
	
Scenario: TC418 - To verify when user clicks cancel after entering all the data in inline table 1
Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC418_Table | TC418_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC418_PPT | TC418_Table | 		
	When User clicks on presentation "TC418_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Enter the data in inline table "SinglelineCol" field as "Text"
	And Click on cancel
	Then Entered data should not get saved and past data will remain same
	And Click on cancel
	And User deletes the presentation "TC418_PPT"
	And User deletes the table "TC418_Table"
	
	
Scenario: TC610 - Verify Updated inline table 1 details should not get saved when user clicks save button from inline table 1 and cancel button main table
Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC610_Table | TC610_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC610_PPT | TC610_Table | 		
	When User clicks on presentation "TC610_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Enter the data in inline table "SinglelineCol" field as "Text"
	And Save the record
	And Cancel the main table
	And Click on view
	And Close the record
	And User deletes the presentation "TC610_PPT"
	And User deletes the table "TC610_Table"