@P1_Smoke
Feature: Add Inline Table 1

Scenario: TC85 - To verify whether user can enter data in UUID field  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC85_Table | TC85_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC85_PPT | TC85_Table | 		
	When User clicks on presentation "TC85_PPT"
	And Click on plus icon
	And Click on Add record link
	And Try to enter the data in "UUID" field of inline table
	Then User should not be able to enter data in "UUID" field of inline table
	And User deletes the presentation "TC85_PPT"
	And User deletes the table "TC85_Table"
	
	
Scenario: TC87 - To verify whether user can enter data in 'Inline table 1 singleline' field
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC87_Table | TC87_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC87_PPT | TC87_Table | 		
	When User clicks on presentation "TC87_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	Then User should be able to enter data "DemoText" in inline table "SinglelineCol" field 
	And User deletes the presentation "TC87_PPT"
	And User deletes the table "TC87_Table"
	
	
Scenario: TC100 - To verify whether user can enter data in 'Inline table 1 Numeric' field 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC100_Table | TC100_InlineTable |NumericCol | Numeric |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC100_PPT | TC100_Table | 		
	When User clicks on presentation "TC100_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "NumericCol" field as "123"
	Then User should be able to enter data "123" in inline table "NumericCol" field 
	And User deletes the presentation "TC100_PPT"
	And User deletes the table "TC100_Table"
	
	
Scenario: TC93 - To verify whether user can enter data in 'Inline Table 1 MultiLine' field
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC93_Table | TC93_InlineTable |MultilineCol | Multiline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC93_PPT | TC93_Table | 		
	When User clicks on presentation "TC93_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	Then User should be able to enter data "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo " inline table "MultilineCol" field
	And User deletes the presentation "TC93_PPT"
	And User deletes the table "TC93_Table"
	
	
Scenario: TC111 - To verify whether user can attach image using + button from 'Inline table 1 image' 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC111_Table | TC111_InlineTable |AttachImage | image cell |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC111_PPT | TC111_Table | 		
	When User clicks on presentation "TC111_PPT"
	And Click on plus icon
	And Click on Add record link
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG" in inline table
	Then User should be able to upload the attachment in inline table
	And User deletes the presentation "TC111_PPT"
	And User deletes the table "TC111_Table"
	
	
Scenario: TC117 - To verify whether  user can attach files in 'Inline table 1 attachment' field 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC117_Table | TC117_InlineTable |Attachment | Attachment |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC117_PPT | TC117_Table | 		
	When User clicks on presentation "TC117_PPT"
	And Click on plus icon
	And Click on Add record link
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Sample.txt" in inline table
	Then User should be able to upload the attachment in inline table
	And User deletes the presentation "TC117_PPT"
	And User deletes the table "TC117_Table"
	
	
Scenario: TC132 - To verify whether user can enter URL link in 'Inline table 1 URL' field 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC132_Table | TC132_InlineTable |URLCol | URL |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC132_PPT | TC132_Table | 		
	When User clicks on presentation "TC132_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in "URLCol" field as "www.google.com" in inline table
	Then User should be able to enter data "www.google.com" in "URLCol" field in inline table
	And User deletes the presentation "TC132_PPT"
	And User deletes the table "TC132_Table"
	
	
Scenario: TC136 - To verify whether user can enter data in 'Inline table 1 auto' field  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC136_Table | TC136_InlineTable |AutoGenerateCol | Auto Generated |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC136_PPT | TC136_Table | 		
	When User clicks on presentation "TC132_PPT"
	And Click on plus icon
	And Click on Add record link
	And Save the record
	And Save the record
	And Click on view
	Then Key should be auto generated
	And Close the record
	And User deletes the presentation "TC136_PPT"
	And User deletes the table "TC136_Table"
	
	
Scenario: TC831 - Allow to View - Inline table - Singleline Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC831_Table | TC831_InlineTable |SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC831_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Checks whether "Allow to View" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC831_PPT | TC831_Table | 		
	When User clicks on presentation "TC831_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "SinglelineCol" column should be displayed
	And Click on cancel
	And User deletes the presentation "TC831_PPT"
	And User deletes the table "TC831_Table"
	
	
Scenario: TC832 - Allow to View - Inline table - Singleline Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC832_Table | TC832_InlineTable |SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC832_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Uncheck the "Allow to View" option	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC832_PPT | TC832_Table | 		
	When User clicks on presentation "TC832_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "SinglelineCol" column should not be displayed
	And Click on cancel
	And User deletes the presentation "TC832_PPT"
	And User deletes the table "TC832_Table"
	
		
Scenario: TC835 - Allow to View - Inline table - Numeric Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC835_Table | TC835_InlineTable |NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC835_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Checks whether "Allow to View" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC835_PPT | TC835_Table | 		
	When User clicks on presentation "TC835_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "NumericCol" column should be displayed
	And Click on cancel
	And User deletes the presentation "TC835_PPT"
	And User deletes the table "TC835_Table"
	
	
Scenario: TC836 - Allow to View - Inline table - Numeric Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC836_Table | TC836_InlineTable |NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC836_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Uncheck the "Allow to View" option	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC836_PPT | TC836_Table | 		
	When User clicks on presentation "TC836_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "NumericCol" column should not be displayed
	And Click on cancel
	And User deletes the presentation "TC836_PPT"
	And User deletes the table "TC836_Table"
	
	

Scenario: TC837 - Allow to View - Inline table - Image cell Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC837_Table | TC837_InlineTable | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC837_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Checks whether "Allow to View" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC837_PPT | TC837_Table | 		
	When User clicks on presentation "TC837_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "AttachImage" column should be displayed for Image cell
	And Click on cancel
	And User deletes the presentation "TC837_PPT"
	And User deletes the table "TC837_Table"
	
	
Scenario: TC838 - Allow to View - Inline table - Image cell Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC838_Table | TC838_InlineTable | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC838_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Checks whether "Allow to View" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC838_PPT | TC838_Table | 		
	When User clicks on presentation "TC838_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "AttachImage" column should be displayed for Image cell
	And Click on cancel
	And User deletes the presentation "TC838_PPT"
	And User deletes the table "TC838_Table"	
	
	
Scenario: TC839 - Allow to View - Inline table - Attachment Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC839_Table | TC839_InlineTable | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC839_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Checks whether "Allow to View" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC839_PPT | TC839_Table | 		
	When User clicks on presentation "TC839_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "AttachmentCol" column should be displayed for Image cell
	And Click on cancel
	And User deletes the presentation "TC839_PPT"
	And User deletes the table "TC839_Table"
	
	
Scenario: TC840 - Allow to View - Inline table - Attachment Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC840_Table | TC840_InlineTable | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC840_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Checks whether "Allow to View" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC840_PPT | TC840_Table | 		
	When User clicks on presentation "TC840_PPT"
	And Click on plus icon
	And Click on Add record link
	Then "AttachmentCol" column should be displayed for Image cell
	And Click on cancel
	And User deletes the presentation "TC840_PPT"
	And User deletes the table "TC840_Table"



Scenario: TC863 - Allow to Edit - Inline table - Singleline Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC863_Table | TC863_InlineTable |SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC863_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC863_PPT | TC863_Table | 		
	When User clicks on presentation "TC863_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	Then User should be able to enter data "DemoText" in inline table "SinglelineCol" field
	And User deletes the presentation "TC863_PPT"
	And User deletes the table "TC863_Table"
	
		
Scenario: TC864 - Allow to Edit - Inline table - Singleline Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC864_Table | TC864_InlineTable |SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC864_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Uncheck the "Allow to Edit" option	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC864_PPT | TC864_Table | 		
	When User clicks on presentation "TC864_PPT"
	And Click on plus icon
	And Click on Add record link
	And Try to enter the data in "SinglelineCol" field
	Then User should not be able to enter data in "SinglelineCol" field
	And Click on cancel
	And User deletes the presentation "TC864_PPT"
	And User deletes the table "TC864_Table"
	

Scenario: TC865 - Allow to Edit - Inline table - Multiline Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC865_Table | TC865_InlineTable |MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC865_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC865_PPT | TC865_Table | 		
	When User clicks on presentation "TC865_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	Then User should be able to enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And Click on cancel
	And User deletes the presentation "TC865_PPT"
	And User deletes the table "TC865_Table"
	
		
Scenario: TC866 - Allow to Edit - Inline table - Singleline Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC866_Table | TC866_InlineTable |MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC866_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Uncheck the "Allow to Edit" option	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC866_PPT | TC866_Table | 		
	When User clicks on presentation "TC866_PPT"
	And Click on plus icon
	And Click on Add record link
	And Try to enter in "MultilineCol" field
	Then User should not be able to enter data in "MultilineCol" field
	And Click on cancel
	And User deletes the presentation "TC866_PPT"
	And User deletes the table "TC866_Table"
	
	
		
Scenario: TC867 - Allow to Edit - Inline table - Numeric Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC867_Table | TC867_InlineTable |NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC867_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC867_PPT | TC867_Table | 		
	When User clicks on presentation "TC867_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "NumericCol" field as "123"
	Then User should be able to enter data "123" in inline table "NumericCol" field
	And User deletes the presentation "TC867_PPT"
	And User deletes the table "TC867_Table"
	
	
Scenario: TC868 - Allow to Edit - Inline table - Numeric Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC868_Table | TC868_InlineTable |NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC868_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Uncheck the "Allow to Edit" option	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC868_PPT | TC868_Table | 		
	When User clicks on presentation "TC868_PPT"
	And Click on plus icon
	And Click on Add record link
	And Try to enter the data in "NumericCol" field
	Then User should not be able to enter data in "NumericCol" field
	And Click on cancel
	And User deletes the presentation "TC868_PPT"
	And User deletes the table "TC868_Table"
	

Scenario: TC869 - Allow to Edit - Inline table - Image Cell Presentation - Verify the field is displayed if allow to view is set ON	
Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC869_Table | TC869_InlineTable | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC869_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC869_PPT | TC869_Table | 		
	When User clicks on presentation "TC869_PPT"
	And Click on plus icon
	And Click on Add record link
	And Try to upload in "AttachImage" field
	Then User should not be able to enter data in "AttachImage" field
	And Click on cancel
	And User deletes the presentation "TC869_PPT"
	And User deletes the table "TC869_Table"
	
	
Scenario: TC870 - Allow to Edit - Inline table - Image Cell Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC870_Table | TC870_InlineTable | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC870_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC870_PPT | TC870_Table | 		
	When User clicks on presentation "TC870_PPT"
	And Click on plus icon
	And Click on Add record link
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG"
	Then User should be able to upload the attachment
	And Click on cancel
	And User deletes the presentation "TC870_PPT"
	And User deletes the table "TC870_Table"	
	
	
Scenario: TC871 - Allow to Edit - Inline table - Attachment Presentation - Verify the field is displayed if allow to view is set ON 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC871_Table | TC871_InlineTable | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC871_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC871_PPT | TC871_Table | 		
	When User clicks on presentation "TC871_PPT"
	And Click on plus icon
	And Click on Add record link
	And Try to upload in "AttachImage" field
	Then User should not be able to enter data in "AttachImage" field
	And Click on cancel
	And User deletes the presentation "TC871_PPT"
	And User deletes the table "TC871_Table"
	
	
Scenario: TC872 - Allow to Edit - Inline table - Attachment Presentation - Verify the field is displayed if allow to view is set OFF 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC872_Table | TC872_InlineTable | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC872_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Checks whether "Allow to Edit" option is checked	
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC872_PPT | TC872_Table | 		
	When User clicks on presentation "TC872_PPT"
	And Click on plus icon
	And Click on Add record link
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Sample.txt"
	Then User should be able to upload the attachment
	And Click on cancel
	And User deletes the presentation "TC872_PPT"
	And User deletes the table "TC872_Table"	 
	

	

