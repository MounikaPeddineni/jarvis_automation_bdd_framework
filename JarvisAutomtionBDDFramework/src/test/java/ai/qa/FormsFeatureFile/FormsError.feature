@P1_Smoke
Feature: FormsError

Scenario: TC776 - Verify error message is displayed for the singleline column if value is not provided while adding record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC776_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC776_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC776_PPT | TC776_Table | 
	And User clicks on presentation "TC776_PPT"
	And Click on plus icon to add a record
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "SinglelineCol"
	And Click on cancel
	And User deletes the presentation "TC776_PPT"
	And User deletes the table "TC776_Table"
	
	

Scenario: TC737 - Verify error message is displayed for the singleline column if value is not provided while editing record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC737_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC737_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC737_PPT | TC737_Table | 
	And User clicks on presentation "TC737_PPT"
	And Click on plus icon
	And Enter the data in "SinglelineCol" field as "DemoText"
	And Save the record
	And User clicks on edit record button
	And Clear the existing text in "SinglelineCol"
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "SinglelineCol"
	And Click on cancel
	And User deletes the presentation "TC737_PPT"
	And User deletes the table "TC737_Table"
	
	
Scenario: TC738 - Verify error message is displayed for the singleline column if value is not provided while adding record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC738_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC738_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Country"
	And Select the field as "SinglelineCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC738_PPT | TC738_Table | 
	And User clicks on presentation "TC738_PPT"
	And Click on plus icon
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "SinglelineCol"
	And Click on cancel
	And User deletes the presentation "TC738_PPT"
	And User deletes the table "TC738_Table"
	
	
Scenario: TC739 - Verify error message is displayed for the singleline column if value is not provided while editing record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC739_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC739_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Country"
	And Select the field as "SinglelineCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC739_PPT | TC739_Table | 
	And User clicks on presentation "TC739_PPT"
	And Click on plus icon
	And Enter the data in "SinglelineCol" field as "DemoText"
	And Save the record
	And User clicks on edit record button
	And Clear the existing text in "SinglelineCol"
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "SinglelineCol"
	And Click on cancel
	And User deletes the presentation "TC739_PPT"
	And User deletes the table "TC739_Table"
	
	
Scenario: TC741 - Verify error message is displayed for the multiline column if value is not provided while adding record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC741_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC741_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC741_PPT | TC741_Table | 
	And User clicks on presentation "TC741_PPT"
	And Click on plus icon to add a record
	And Save the record
	Then Error message should get displayed as "Field is mandatory" for "MultilineCol"
	And Click on cancel
	And User deletes the presentation "TC741_PPT"
	And User deletes the table "TC741_Table"
	
	
Scenario: TC742 - Verify error message is displayed for the singleline column if value is not provided while editing record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC742_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC742_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC742_PPT | TC742_Table | 
	And User clicks on presentation "TC742_PPT"
	And Click on plus icon
	And Enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And Save the record
	And User clicks on edit record button
	And Clear the text in "MultilineCol"
	And Save the record
	Then Error message should get displayed as "Field is mandatory" for "MultilineCol"
	And Click on cancel
	And User deletes the presentation "TC742_PPT"
	And User deletes the table "TC742_Table"
	
	
Scenario: TC743 - Verify error message is displayed for the multiline column if value is not provided while adding record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC743_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC743_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Country"
	And Select the field as "MultilineCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC743_PPT | TC743_Table | 
	And User clicks on presentation "TC743_PPT"
	And Click on plus icon
	And Save the record
	Then Error message should get displayed as "Field is mandatory" for "MultilineCol"
	And Click on cancel
	And User deletes the presentation "TC743_PPT"
	And User deletes the table "TC743_Table"
	
	
Scenario: TC744 - Verify error message is displayed for the multiline column if value is not provided while editing record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC744_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC744_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Country"
	And Select the field as "MultilineCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC744_PPT | TC744_Table | 
	And User clicks on presentation "TC744_PPT"
	And Click on plus icon
	And Enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And Save the record
	And User clicks on edit record button
	And Clear the text in "MultilineCol"
	And Save the record
	Then Error message should get displayed as "Field is mandatory" for "MultilineCol"
	And Click on cancel
	And User deletes the presentation "TC744_PPT"
	And User deletes the table "TC744_Table"
	
	
Scenario: TC746 - Verify error message is displayed for the Numeric column if value is not provided while adding record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC746_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC746_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC746_PPT | TC746_Table | 
	And User clicks on presentation "TC746_PPT"
	And Click on plus icon to add a record
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "NumericCol"
	And Click on cancel
	And User deletes the presentation "TC746_PPT"
	And User deletes the table "TC746_Table"
	
	
Scenario: TC747 - Verify error message is displayed for the Numeric column if value is not provided while editing record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC747_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC747_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC747_PPT | TC747_Table | 
	And User clicks on presentation "TC747_PPT"
	And Click on plus icon
	And Enter the data in "NumericCol" field as "123"
	And Save the record
	And User clicks on edit record button
	And Clear the existing text in "NumericCol"
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "NumericCol"
	And Click on cancel
	And User deletes the presentation "TC747_PPT"
	And User deletes the table "TC747_Table"
	
	
Scenario: TC748 - Verify error message is displayed for the Numeric column if value is not provided while adding record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC748_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC748_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "CountryCode"
	And Select the field as "NumericCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC748_PPT | TC748_Table | 
	And User clicks on presentation "TC748_PPT"
	And Click on plus icon
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "NumericCol"
	And Click on cancel
	And User deletes the presentation "TC748_PPT"
	And User deletes the table "TC748_Table"
	
	
Scenario: TC749 - Verify error message is displayed for the Numeric column if value is not provided while editing record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC749_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC749_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "CountryCode"
	And Select the field as "NumericCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC749_PPT | TC749_Table | 
	And User clicks on presentation "TC749_PPT"
	And Click on plus icon
	And Enter the data in "NumericCol" field as "123"
	And Save the record
	And User clicks on edit record button
	And Clear the existing text in "NumericCol"
	And Save the record
	Then Error message should be displayed as "Field is mandatory" for "NumericCol"
	And Click on cancel
	And User deletes the presentation "TC749_PPT"
	And User deletes the table "TC749_Table"
	
	
	
Scenario: TC751 - Verify error message is displayed for the Image column if value is not provided while adding record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC751_Table | ImageCol | image cell |	
		
	When User navigate to forms and select the table "TC751_Table"
	And User go to the field settings tab
	And clicks on settings icon for "ImageCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC751_PPT | TC751_Table | 
	And User clicks on presentation "TC751_PPT"
	And Click on plus icon to add a record
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC751_PPT"
	And User deletes the table "TC751_Table"
	
	
	
Scenario: TC752 - Verify error message is displayed for the Image column if value is not provided while editing record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC752_Table | ImageCol | image cell |	
		
	When User navigate to forms and select the table "TC752_Table"
	And User go to the field settings tab
	And clicks on settings icon for "ImageCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC752_PPT | TC752_Table | 
	And User clicks on presentation "TC752_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG"
	And Save the record
	And User clicks on edit record button
	And Delete the existing image
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC752_PPT"
	And User deletes the table "TC752_Table"
	
	
	
Scenario: TC753 - Verify error message is displayed for the Image column if value is not provided while adding record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC753_Table | ImageCol | image cell |	
		
	When User navigate to forms and select the table "TC753_Table"
	And User go to the field settings tab
	And clicks on settings icon for "ImageCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Images"
	And Select the field as "ImageCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC753_PPT | TC753_Table | 
	And User clicks on presentation "TC753_PPT"
	And Click on plus icon
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC753_PPT"
	And User deletes the table "TC753_Table"
	
	
	
Scenario: TC754 - Verify error message is displayed for the Image column if value is not provided while editing record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC754_Table | ImageCol | image cell |	
		
	When User navigate to forms and select the table "TC754_Table"
	And User go to the field settings tab
	And clicks on settings icon for "ImageCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Images"
	And Select the field as "ImageCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC754_PPT | TC754_Table | 
	And User clicks on presentation "TC754_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG"
	And Save the record
	And User clicks on edit record button
	And Delete the existing image
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC754_PPT"
	And User deletes the table "TC754_Table"
	
	
Scenario: TC756 - Verify error message is displayed for the Attachment column if value is not provided while adding record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC756_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC756_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC756_PPT | TC756_Table | 
	And User clicks on presentation "TC756_PPT"
	And Click on plus icon to add a record
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC756_PPT"
	And User deletes the table "TC756_Table"
	
	
Scenario: TC757 - Verify error message is displayed for the Attachment column if value is not provided while editing record 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC757_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC757_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Check "Mark as mandatory" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC757_PPT | TC757_Table | 
	And User clicks on presentation "TC757_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Sample.txt"
	And Save the record
	And User clicks on edit record button
	And Delete the existing image
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC757_PPT"
	And User deletes the table "TC757_Table"
	
	
Scenario: TC758 - Verify error message is displayed for the Attachment column if value is not provided while adding record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC758_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC758_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Attachements"
	And Select the field as "AttachmentCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC758_PPT | TC758_Table | 
	And User clicks on presentation "TC758_PPT"
	And Click on plus icon
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC758_PPT"
	And User deletes the table "TC758_Table"


Scenario: TC759 - Verify error message is displayed for the Attachment column if value is not provided while editing record in section 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC759_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC759_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Check "Mark as mandatory" option
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Attachements"
	And Select the field as "AttachmentCol"
	And Click on create
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC759_PPT | TC759_Table | 
	And User clicks on presentation "TC759_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Sample.txt"
	And Save the record
	And User clicks on edit record button
	And Delete the existing image
	And Save the record
	And Click on error icon
	Then Error message should be displayed as "This field should not be left blank."
	And Click on cancel
	And User deletes the presentation "TC759_PPT"
	And User deletes the table "TC759_Table"

