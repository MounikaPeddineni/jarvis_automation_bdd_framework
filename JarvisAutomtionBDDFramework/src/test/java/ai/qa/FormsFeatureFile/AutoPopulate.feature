@P1_Smoke
Feature: Auto Populate

Scenario: TC879 - Verify user is able to set auto populated for a singleline type column
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC879Auto_Table | CountryAutoPopulate | Singleline |			
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC879LookUp_Table | Country | Singleline |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC879LookUp_PPT | TC879LookUp_Table |	
	And Click on plus icon
	And Enter the data in "Country" field as "India"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "USA"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "UK"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "Canada"
	And User click on save button	
	When User navigate to forms and select the table "TC879Auto_Table"
	And User go to the field settings tab
	And clicks on auto population settings icon for "CountryAutoPopulate" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|
		|TC879LookUp_Table|Country|Country|Ascending|
	And Save the autopopulate		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC879Auto_PPT | TC879Auto_Table |		
	And Click on plus icon
	And Clicks on "CountryAutoPopulate" column
	And Select the country as "India"
	And Click on save
	Then User should be able to select the "India"
	And User deletes the presentation "TC879Auto_PPT"
	And User deletes the table "TC879Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC879LookUp_PPT"
	And User deletes the table "TC879LookUp_Table"
	
	
Scenario: TC881 - Verify user is able to set auto populated for a numeric type column
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC881Auto_Table | QuantityAutoPopulate | Numeric |			
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC881LookUp_Table | Quantity | Numeric |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC881LookUp_PPT | TC881LookUp_Table |		
	And Click on plus icon
	And Enter the data in "Quantity" field as "1"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "2"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "3"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "4"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "5"
	And User click on save button	
	When User navigate to forms and select the table "TC881Auto_Table"
	And User go to the field settings tab
	And clicks on auto population settings icon for "QuantityAutoPopulate" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|
		|TC881LookUp_Table|Quantity|Quantity|Ascending|
	And Save the autopopulate	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC881Auto_PPT | TC881Auto_Table |		
	And Click on plus icon
	And Clicks on "QuantityAutoPopulate" column
	And Select the country as "2"
	And Click on save
	Then User should be able to select the "2"
	And User deletes the presentation "TC881Auto_PPT"
	And User deletes the table "TC881Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC881LookUp_PPT"
	And User deletes the table "TC881LookUp_Table"
	
	
Scenario: TC882 - Verify auto populate works without condition
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC882Auto_Table | NameAutoPopulate | Singleline |			
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC882LookUp_Table | Name | Singleline |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC882LookUp_PPT | TC882LookUp_Table |	
	And Click on plus icon
	And Enter the data in "Name" field as "John"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "Stephen"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "David"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "Jackson"
	And User click on save button	
	When User navigate to forms and select the table "TC882Auto_Table"
	And User go to the field settings tab
	And clicks on auto population settings icon for "NameAutoPopulate" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|
		|TC882LookUp_Table|Name|Name|Ascending|
	And Save the autopopulate		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC882Auto_PPT | TC882Auto_Table |		
	And Click on plus icon
	And Clicks on "NameAutoPopulate" column
	And Select the country as "David"
	And Click on save
	Then User should be able to select the "David"
	And User deletes the presentation "TC882Auto_PPT"
	And User deletes the table "TC882Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC882LookUp_PPT"
	And User deletes the table "TC882LookUp_Table"

Scenario: TC883 - Verify auto populate works for greater than operator with constant
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC883Auto_Table | QuantityAutoPopulate | Numeric |			
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC883LookUp_Table | Quantity | Numeric |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC883LookUp_PPT | TC883LookUp_Table |
	And Click on plus icon
	And Enter the data in "Quantity" field as "1"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "2"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "3"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "4"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Quantity" field as "5"
	And User click on save button
	When User navigate to forms and select the table "TC883Auto_Table"
	And User go to the field settings tab
	And clicks on auto population settings icon for "QuantityAutoPopulate" column
	And Adds the auto population with the condition
		|LookUpTable|FieldName|OrderByField|OrderByType|Field|Operator|Constant|
		|TC883LookUp_Table|Quantity|Quantity|Ascending|Quantity|greater than or equal|3|
	And Save the autopopulate
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC883Auto_PPT | TC883Auto_Table |
	And Click on plus icon
	And Clicks on "QuantityAutoPopulate" column
	Then Quantity greater than or equal "3" should be displayed	
	And Click on cancel
	And User deletes the presentation "TC883Auto_PPT"
	And User deletes the table "TC883Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC883LookUp_PPT"
	And User deletes the table "TC883LookUp_Table"



Scenario: TC902 - Verify auto populate is applied for columns added under sections 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC902Auto_Table | CountryAutoPopulate | Singleline |	
		
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC902LookUp_Table | Country | Singleline |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC902LookUp_PPT | TC902LookUp_Table |
		
	And Click on plus icon
	And Enter the data in "Country" field as "India"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "USA"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "UK"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "Canada"
	And User click on save button		
	When User navigate to forms and select the table "TC902Auto_Table"
	And User go to the field settings tab
	And Click on plus icon to add a column
	And Click on Add Sections
	And Enter the section name as "Country"
	And Select the field as "CountryAutoPopulate"
	And Click on create
	And Click on save
	And clicks on auto population settings icon for "CountryAutoPopulate" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|
		|TC902LookUp_Table|Country|Country|Ascending|
	And Save the autopopulate		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC902Auto_PPT | TC902Auto_Table |		
	And Click on plus icon
	And Clicks on "CountryAutoPopulate" column
	And Select the country as "India"
	And Click on save
	Then User should be able to select the "India"
	And User deletes the presentation "TC902Auto_PPT"
	And User deletes the table "TC902Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC902LookUp_PPT"
	And User deletes the table "TC902LookUp_Table"
	
	
Scenario: TC908 - Verify user is able to enter value for an auto populated singleline type column 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC908Auto_Table | CountryAutoPopulate | Singleline |	
		
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC908LookUp_Table | Country | Singleline |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC908LookUp_PPT | TC908LookUp_Table |
		
	And Click on plus icon
	And Enter the data in "Country" field as "India"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "USA"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "UK"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "Canada"
	And User click on save button		
	When User navigate to forms and select the table "TC908Auto_Table"
	And User go to the field settings tab
	And clicks on auto population settings icon for "CountryAutoPopulate" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|
		|TC908LookUp_Table|Country|Country|Ascending|	
	And Check the "Allow user to override auto-populated values" checkbox
	And Save the autopopulate	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC908Auto_PPT | TC908Auto_Table |		
	And Click on plus icon
	And Clicks on "CountryAutoPopulate" column
	And Enter the data as "Germany"
	And Click on save
	Then User should be able to select the "Germany"
	And User deletes the presentation "TC908Auto_PPT"
	And User deletes the table "TC908Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC908LookUp_PPT"
	And User deletes the table "TC908LookUp_Table"
	
	
	
Scenario: TC912 - Verify if "Fill the first record by default" is checked, first value is automatically selected in the auto population field 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC912Auto_Table | CountryAutoPopulate | Singleline |	
		
	And User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC912LookUp_Table | Country | Singleline |		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC912LookUp_PPT | TC912LookUp_Table |
		
	And Click on plus icon
	And Enter the data in "Country" field as "India"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "USA"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "UK"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Country" field as "Canada"
	And User click on save button		
	When User navigate to forms and select the table "TC912Auto_Table"
	And User go to the field settings tab
	And clicks on auto population settings icon for "CountryAutoPopulate" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|
		|TC912LookUp_Table|Country|Country|Ascending|	
	And Check the "Fill the first record by default" checkbox
	And Save the autopopulate	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC912Auto_PPT | TC912Auto_Table |		
	And Click on plus icon
	Then "Canada" is selected by default
	And Click on save
	And User deletes the presentation "TC912Auto_PPT"
	And User deletes the table "TC912Auto_Table"
	And Refresh the page
	And User deletes the presentation "TC912LookUp_PPT"
	And User deletes the table "TC912LookUp_Table"
	
	
Scenario: Verify auto populate works with multiple conditions for a single field with variable
	Given User create a Jiffy table with multiple columns  for following details
		| tableName               |  columnName1            | columnType1 |  columnName2       | columnType2|  columnName3   | columnType3 |
		| TC259_AutoPopulateTable | AutopopulateSingleline | Singleline | AutopopulateMultiline |	Multiline | AupopulateNumeric | Numeric |					
								
	
		
	And User create a Jiffy table with multiple columns  for following details
		| tableName  |  columnName1  | columnType1 |columnName2  | columnType2 | columnName3  | columnType3 |
		| TC259_LookUpTable | Name   | Singleline  | Address     | Multiline   |Number        | Numeric |
							
						
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC259_LookUpPPT | TC259_LookUpTable |
	
	And Click on plus icon
	And Enter the data in "Name" field as "John"
	And Enter the data in "Number" field as "2000"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "Sam"
	And Enter the data in "Number" field as "3000"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "David"
	And Enter the data in "Number" field as "6000"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "Joseph"
	And Enter the data in "Number" field as "1000"
	And User click on save button
	And Click on plus icon
	And Enter the data in "Name" field as "John"
	And Enter the data in "Number" field as "200"
	And User click on save button
	When User navigate to forms and select the table "TC259_AutoPopulateTable"
	And User go to the field settings tab
	And clicks on auto population settings icon for "AutopopulateSingleline" column
	And Adds the auto population with the following details
		|LookUpTable|FieldName|OrderByField|OrderByType|Field|Operator|Constant|
		|TC259_LookUpTable|Number|Number|Ascending|number|not equal|6000|
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC259_AutoPopulatePPT | TC259_AutoPopulate |
	And Click on plus icon
	And Clicks on "Name" column
	Then The names having number "2000" should be displayed
	
	