@P1_Smoke
Feature: Custom Button

Scenario: TC730 - Verify execution state can be changed after clicking on the custom button
	Given User create a Jiffy table with table name "TC730_Table"
	When User navigate to forms and select the table "TC730_Table"
	And Clicks on button settings
	And Add a custom button with name "CLOSE"
	And Select the execution state as "Completed"
	And Select the action as "SAVE AND EXIT"
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC730_PPT | TC730_Table |
	And Click on plus icon
	And Click on "CLOSE" button
	And Save the record
	Then Execution state will be changed to "Completed"
	And User deletes the presentation "TC730_PPT"
	And User deletes the table "TC730_Table"
	
	
Scenario: TC731 - Verify save action for a custom button
	Given User create a Jiffy table with table name "TC731_Table"
	When User navigate to forms and select the table "TC731_Table"
	And Clicks on button settings
	And Add a custom button with name "SaveBtn"
	And Select the action as "SAVE"
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC731_PPT | TC731_Table |
	And Click on plus icon
	And Click on "Execution State" field
	And Select the state "Inprogress"
	And Click on "SaveBtn" button
	And Save the record
	Then Execution state will be saved as "Inprogress"
	And User deletes the presentation "TC731_PPT"
	And User deletes the table "TC731_Table"
	
	
Scenario: TC732 - Verify exit action for a custom button
	Given User create a Jiffy table with table name "TC732_Table"
	When User navigate to forms and select the table "TC732_Table"
	And Clicks on button settings
	And Add a custom button with name "Exit"
	And Select the action as "EXIT"
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC732_PPT | TC732_Table |
	And Click on plus icon
	And Click on "Execution State" field
	And Select the state "Inprogress"
	And Click on "Exit" button
	And Save the record
	Then Form is closed without saving the changes
	And User deletes the presentation "TC732_PPT"
	And User deletes the table "TC732_Table"
	
	
Scenario: TC733 - Verify save and exit action for a custom button
	Given User create a Jiffy table with table name "TC733_Table"
	When User navigate to forms and select the table "TC733_Table"
	And Clicks on button settings
	And Add a custom button with name "Save&Exit"
	And Select the action as "SAVE AND EXIT"
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC733_PPT | TC733_Table |
	And Click on plus icon
	And Click on "Execution State" field
	And Select the state "Inprogress"
	And Click on "Save&Exit" button
	And Save the record
	Then Form is saved and the window is closed.
	And User deletes the presentation "TC733_PPT"
	And User deletes the table "TC733_Table"
