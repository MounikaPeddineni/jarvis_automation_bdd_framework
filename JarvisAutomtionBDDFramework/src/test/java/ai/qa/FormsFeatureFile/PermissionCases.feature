@P1_Smoke
Feature: Permission Cases

Scenario: TC695 - Designer User - Verify user is able to add record only if New Entry creation is provided
	Given User create a Jiffy table with table name "TC695_Table"
	When User navigate to forms and select the table "TC695_Table"
	And Select the role as "Designer"
	And User do not select any state
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC695_PPT | TC695_Table |
	And Click on plus icon
	Then User should not be able to add the record
	And User deletes the presentation "TC695_PPT"
	And User deletes the table "TC695_Table"
	
	
Scenario: TC696 - Designer User - Verify user is able to edit record for execution state as New
	Given User create a Jiffy table with table name "TC696_Table"
	When User navigate to forms and select the table "TC696_Table"
	And Select the role as "Designer"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC696_PPT | TC696_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "New"
	And Save the record
	And User navigate to forms and select the table "TC696_Table"
	And Select the state as "New"
	And Click on save
	And User navigates to presentation "TC696_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC696_PPT"
	And User deletes the table "TC696_Table"
	
	
	
Scenario: TC697 - Designer User - Verify user is able to edit record for execution state as In Progress
	Given User create a Jiffy table with table name "TC697_Table"
	When User navigate to forms and select the table "TC697_Table"
	And Select the role as "Designer"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC697_PPT | TC697_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "Inprogress"
	And Save the record
	And User navigate to forms and select the table "TC697_Table"
	And Select the state as "Inprogress"
	And Click on save
	And User navigates to presentation "TC697_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC697_PPT"
	And User deletes the table "TC697_Table"
	
	
Scenario: TC698 - Designer User - Verify user is able to edit record for execution state as Completed
	Given User create a Jiffy table with table name "TC698_Table"
	When User navigate to forms and select the table "TC698_Table"
	And Select the role as "Designer"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC698_PPT | TC698_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "Completed"
	And Save the record
	And User navigate to forms and select the table "TC698_Table"
	And Select the state as "Completed"
	And Click on save
	And User navigates to presentation "TC698_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC698_PPT"
	And User deletes the table "TC698_Table"
	
	
Scenario: TC699 - Designer User - Verify user is able to edit record for custom execution state
	Given User create a Jiffy table with table name "TC699_Table" and custom execution state "Updated"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC699_PPT | TC699_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "Updated"
	And Save the record
	When User navigate to forms and select the table "TC699_Table"
	And Select the role as "Designer"
	And Select the custom state "Updated"
	And Click on save
	And User navigates to presentation "TC699_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC699_PPT"
	And User deletes the table "TC699_Table"
	
	
Scenario: TC707 - Business User - Verify user is not able to add record if permission is not given
	Given User create a Jiffy table with table name "TC707_Table"
	When User navigate to forms and select the table "TC707_Table"
	And Select the role as "BusinessUser"
	And User do not select any state
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC707_PPT | TC707_Table |
	And Click on plus icon
	Then User should not be able to add the record
	And User deletes the presentation "TC707_PPT"
	And User deletes the table "TC707_Table"
	
	
	
Scenario: TC709 - Business User - Verify user is able to add record only if New Entry creation is provided
	Given User create a Jiffy table with table name "TC709_Table"
	When User navigate to forms and select the table "TC709_Table"
	And Select the role as "Business"
	And User do not select any state
	And Click on save
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC709_PPT | TC709_Table |
	And Click on plus icon to add a record
	Then User should not be able to add the record
	When User navigate to forms and select the table "TC709_Table"
	And Select the state as "New Entry Creation"
	And Click on save
	And User navigates to presentation "TC709_PPT"
	And Click on plus icon to add a record
	Then User is able to add record
	And Click on cancel
	And User deletes the presentation "TC709_PPT"
	And User deletes the table "TC709_Table"
	
	
Scenario: TC710 - Business User - Verify user is able to edit record for execution state as New
	Given User create a Jiffy table with table name "TC710_Table"
	When User navigate to forms and select the table "TC710_Table"
	And Select the role as "Business"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC710_PPT | TC710_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "New"
	And Save the record
	And User navigate to forms and select the table "TC710_Table"
	And Select the state as "New"
	And Click on save
	And User navigates to presentation "TC710_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC710_PPT"
	And User deletes the table "TC710_Table"
	
	
Scenario: TC711 - Business User - Verify user is able to edit record for execution state as In Progress
	Given User create a Jiffy table with table name "TC711_Table"
	When User navigate to forms and select the table "TC711_Table"
	And Select the role as "Business"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC711_PPT | TC711_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "Inprogress"
	And Save the record
	And User navigate to forms and select the table "TC711_Table"
	And Select the state as "Inprogress"
	And Click on save
	And User navigates to presentation "TC711_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC711_PPT"
	And User deletes the table "TC711_Table"
	
	
Scenario: TC712 - Business User - Verify user is able to edit record for execution state as Completed
	Given User create a Jiffy table with table name "TC712_Table"
	When User navigate to forms and select the table "TC712_Table"
	And Select the role as "Business"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC712_PPT | TC712_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "Completed"
	And Save the record
	And User navigate to forms and select the table "TC712_Table"
	And Select the state as "Completed"
	And Click on save
	And User navigates to presentation "TC712_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC712_PPT"
	And User deletes the table "TC712_Table"
	
	
Scenario: TC713 - Business User - Verify user is able to edit record for custom execution state
	Given User create a Jiffy table with table name "TC713_Table" and custom execution state "Updated"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC713_PPT | TC713_Table |
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "Updated"
	And Save the record
	When User navigate to forms and select the table "TC713_Table"
	And Select the role as "Business"
	And Select the custom state "Updated"
	And Click on save
	And User navigates to presentation "TC713_PPT"
	And User clicks on edit record button
	Then User is able to edit record for the given execution state
	And Click on cancel
	And User deletes the presentation "TC713_PPT"
	And User deletes the table "TC713_Table"