@P1_Smoke
Feature: Cancel Forms

Scenario: TC362 - To verify when user clicks cancel after entering all the data in main table
Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC362_Table | SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC362_PPT | TC362_Table | 		
	When User clicks on presentation "TC362_PPT"
	And Click on plus icon
	And Enter the data in "SinglelineCol" field as "DemoText"
	And Click on cancel
	Then No data should get saved in main table
	And User deletes the presentation "TC362_PPT"
	And User deletes the table "TC362_Table"