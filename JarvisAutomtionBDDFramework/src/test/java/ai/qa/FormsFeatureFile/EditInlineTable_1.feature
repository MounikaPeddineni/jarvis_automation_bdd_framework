@P1_Smoke
Feature: Edit Inline Table 1
Scenario: TC406 - To verify whether user can edit UUID field from inline table 1 screen  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC406_Table | TC406_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC406_PPT | TC406_Table | 		
	When User clicks on presentation "TC406_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Try to enter the data in "UUID" field of inline table
	Then User should not be able to enter data in "UUID" field of inline table
	And User deletes the presentation "TC406_PPT"
	And User deletes the table "TC406_Table"
	
	
Scenario: TC407 - To verify whether user can Edit Singleline field  from inline table 1 screen  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC407_Table | TC407_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC407_PPT | TC407_Table | 		
	When User clicks on presentation "TC407_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Enter the data in inline table "SinglelineCol" field as "Text"
	Then User should be able to enter data "DemoTextText" in inline table "SinglelineCol" field
	And User deletes the presentation "TC407_PPT"
	And User deletes the table "TC407_Table"
	
Scenario: TC408 - To verify whether user can Edit Multiline field  from inline table 1 screen  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC408_Table | TC408_InlineTable | MultilineCol | Multiline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC408_PPT | TC408_Table | 		
	When User clicks on presentation "TC408_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Enter the data in inline table "MultilineCol" field "New Text"
	Then User should be able to enter data "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo New Text" inline table "MultilineCol" field
	And User deletes the presentation "TC408_PPT"
	And User deletes the table "TC408_Table"
	
		
Scenario: TC409 - To verify whether user can Edit Singleline field  from inline table 1 screen  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC409_Table | TC409_InlineTable |NumericCol | Numeric |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC409_PPT | TC409_Table | 		
	When User clicks on presentation "TC409_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "NumericCol" field as "123"
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Enter the data in inline table "NumericCol" field as "456"
	Then User should be able to enter data "123456" in inline table "NumericCol" field
	And User deletes the presentation "TC409_PPT"
	And User deletes the table "TC409_Table"
	
	
Scenario: TC414 - To verify whether user can edit URL field  from inline table 1 screen  
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC414_Table | TC414_InlineTable | URLCol | URL |	
		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC414_PPT | TC414_Table | 		
	When User clicks on presentation "TC414_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "URLCol" field as "https://www.google.com/"
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Enter the data in inline table "URLCol" field as "www.gmail.com"
	Then User should be able to enter data "https://www.google.com/www.gmail.com" in inline table "URLCol" field
	And User deletes the presentation "TC414_PPT"
	And User deletes the table "TC414_Table"


Scenario: TC411 - To verify whether user can attach image using + button from 'Inline table 1 image' 
	Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC411_Table | TC411_InlineTable |AttachImage | image cell |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC411_PPT | TC411_Table | 		
	When User clicks on presentation "TC411_PPT"
	And Click on plus icon
	And Click on Add record link
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG" in inline table
	And Save the data
	And User clicks on edit record button
	And Edit the inline table record
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\LoginBtn.PNG" in inline table
	Then User should be able to upload the attachment in inline table
	And User deletes the presentation "TC411_PPT"
	And User deletes the table "TC411_Table"
	

