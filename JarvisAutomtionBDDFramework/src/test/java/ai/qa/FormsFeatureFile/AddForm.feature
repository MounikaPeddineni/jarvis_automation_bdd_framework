@P1_Smoke
Feature: Add Forms

Scenario: TC19 - To verify whether user can enter data in 'Singleline' field 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC19_Table | SinglelineCol | Singleline |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC19_PPT | TC19_Table | 

	When User clicks on presentation "TC19_PPT"
	And Click on plus icon
	And Enter the data in "SinglelineCol" field as "DemoText"
	Then User should be able to enter data "DemoText" in "SinglelineCol" field 
	And User deletes the presentation "TC19_PPT"
	And User deletes the table "TC19_Table"
	
		
Scenario: TC25 - To verify whether user can enter data in 'MultiLine' field
	Given User create a Jiffy table with following details
		| tableName | columnName | columnType |
		| TC25_Table | MultilineCol | Multiline |
	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC25_PPT | TC25_Table |
		
	When User clicks on presentation "TC25_PPT"
	And Click on plus icon
	And Enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	Then User should be able to enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And User deletes the presentation "TC25_PPT"
	And User deletes the table "TC25_Table"
	
	
Scenario: TC5 - To verify whether user can enter data in UUID field
	Given User create a Jiffy table with table name "TC5_Table"
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC5_PPT | TC5_Table | 

	When User clicks on presentation "TC5_PPT"
	And Click on plus icon to add record
	And Try to enter the data in "UUID" field
	Then User should not be able to enter data in "UUID" field
	And User deletes the presentation "TC5_PPT"
	And User deletes the table "TC5_Table"
	
	
Scenario: TC16 - To verify whether user can select any option from dropdown in 'Execution state' field 
	Given User create a Jiffy table with table name "TC16_Table"	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC16_PPT | TC16_Table | 

	When User clicks on presentation "TC16_PPT"
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "New"
	Then User should be able to select the "New" execution state
	And User deletes the presentation "TC16_PPT"
	And User deletes the table "TC16_Table"
	
	
Scenario: TC32 - To verify whether user can enter data in 'Numeric' field  
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC32_Table | NumericCol | Numeric |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC32_PPT | TC32_Table | 

	When User clicks on presentation "TC32_PPT"
	And Click on plus icon
	And Enter the data in "NumericCol" field as "123"
	Then User should be able to enter data "123" in "NumericCol" field 
	And User deletes the presentation "TC32_PPT"
	And User deletes the table "TC32_Table"
	
	
Scenario: TC43 - To verify whether user can attach image using + button from image field   
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC43_Table | AttachImage | image cell |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC43_PPT | TC43_Table | 

	When User clicks on presentation "TC43_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG"
	Then User should be able to upload the attachment
	And User deletes the presentation "TC43_PPT"
	And User deletes the table "TC43_Table"
	
	
Scenario: TC49 - To verify whether  user can attach files in attachment field   
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC49_Table | AttachmentCol | Attachment |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC49_PPT | TC49_Table | 

	When User clicks on presentation "TC49_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Sample.txt"
	Then User should be able to upload the attachment
	And User deletes the presentation "TC49_PPT"
	And User deletes the table "TC49_Table"
	
	
Scenario: TC64 - To verify whether user can enter URL link in 'URL' field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC64_Table | URLCol | URL |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC64_PPT | TC64_Table |
	When User clicks on presentation "TC64_PPT"
	And Click on plus icon
	And Enter the data in "URLCol" field as "www.google.com"
	Then User should be able to enter data "www.google.com" in "URLCol" field 
	And User deletes the presentation "TC64_PPT"
	And User deletes the table "TC64_Table"
	

Scenario: TC69 - To verify whether the auto generate key is happening or not after save table
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC69_Table | AutoGenerateCol | Auto Generated |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC69_PPT | TC69_Table |
	When User clicks on presentation "TC69_PPT"
	And Click on plus icon
	And Save the record
	Then Key should be auto generated
	And User deletes the presentation "TC69_PPT"
	And User deletes the table "TC69_Table"
	
	
Scenario: TC75- To verify whether user can save table with date and without time  
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC75_Table | DateTimeCol | DateTime |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC75_PPT | TC75_Table |
	When User clicks on presentation "TC75_PPT"
	And Click on plus icon
	And Enter the date in "DateTimeCol" field as "22"
	And Save the record
	Then User should be able to save the record
	And User deletes the presentation "TC75_PPT"
	And User deletes the table "TC75_Table"



Scenario: TC815- Verify the field is displayed if allow to view is set ON for Singleline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC815_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC815_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Checks whether "Allow to View" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC815_PPT | TC815_Table | 
	And User clicks on presentation "TC815_PPT"
	And Click on plus icon to add a record
	Then "SinglelineCol" column should be displayed
	And User deletes the presentation "TC815_PPT"
	And User deletes the table "TC815_Table"
	
	
Scenario: TC816- Verify the field is not  displayed if allow to view is set OFF for Singleline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC816_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC816_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Uncheck the "Allow to View" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC816_PPT | TC816_Table | 
	And User clicks on presentation "TC816_PPT"
	And Click on plus icon to add a record
	Then "SinglelineCol" column should not be displayed
	And User deletes the presentation "TC816_PPT"
	And User deletes the table "TC816_Table"
	
	
Scenario: TC817- Verify the field is displayed if allow to view is set ON for Multiline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC817_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC817_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Checks whether "Allow to View" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC817_PPT | TC817_Table | 
	And User clicks on presentation "TC817_PPT"
	And Click on plus icon to add a record
	Then "MultilineCol" column should get displayed
	And User deletes the presentation "TC817_PPT"
	And User deletes the table "TC817_Table"

Scenario: TC818- Verify the field is not  displayed if allow to view is set OFF for Multiline field 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC818_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC818_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Uncheck the "Allow to View" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC818_PPT | TC818_Table | 
	And User clicks on presentation "TC818_PPT"
	And Click on plus icon to add a record
	Then "MultilineCol" column should not get displayed
	And User deletes the presentation "TC818_PPT"
	And User deletes the table "TC818_Table"
	
	
Scenario: TC819- Verify the field is displayed if allow to view is set ON for Numeric field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC819_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC819_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Checks whether "Allow to View" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC819_PPT | TC819_Table | 
	And User clicks on presentation "TC819_PPT"
	And Click on plus icon to add a record
	Then "NumericCol" column should be displayed
	And User deletes the presentation "TC819_PPT"
	And User deletes the table "TC819_Table"
	
	
Scenario: TC820- Verify the field is not  displayed if allow to view is set OFF for Numeric field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC820_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC820_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Uncheck the "Allow to View" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC820_PPT | TC820_Table | 
	And User clicks on presentation "TC820_PPT"
	And Click on plus icon to add a record
	Then "NumericCol" column should not be displayed
	And User deletes the presentation "TC820_PPT"
	And User deletes the table "TC820_Table"
	
	
Scenario: TC821- Verify the field is displayed if allow to view is set ON for image cell field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC821_Table | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC821_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Checks whether "Allow to View" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC821_PPT | TC821_Table | 
	And User clicks on presentation "TC821_PPT"
	And Click on plus icon to add a record
	Then "AttachImage" column should be displayed for Image cell
	And User deletes the presentation "TC821_PPT"
	And User deletes the table "TC821_Table"
	
	
Scenario: TC822- Verify the field is not  displayed if allow to view is set OFF for image cell field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC822_Table | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC822_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Uncheck the "Allow to View" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC822_PPT | TC822_Table | 
	And User clicks on presentation "TC822_PPT"
	And Click on plus icon to add a record
	Then "AttachImage" column should not be displayed for Image cell
	And User deletes the presentation "TC822_PPT"
	And User deletes the table "TC822_Table"
	
	
Scenario: TC823- Verify the field is displayed if allow to view is set ON for Atachment field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC823_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC823_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Checks whether "Allow to View" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC823_PPT | TC823_Table | 
	And User clicks on presentation "TC823_PPT"
	And Click on plus icon to add a record
	Then "AttachmentCol" column should be displayed for Image cell
	And User deletes the presentation "TC823_PPT"
	And User deletes the table "TC823_Table"
	
	
Scenario: TC824- Verify the field is not  displayed if allow to view is set OFF for Atachment field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC824_Table | AttachmentCol | image cell |	
		
	When User navigate to forms and select the table "TC824_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Uncheck the "Allow to View" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC824_PPT | TC824_Table | 
	And User clicks on presentation "TC824_PPT"
	And Click on plus icon to add a record
	Then "AttachmentCol" column should not be displayed for Image cell
	And User deletes the presentation "TC824_PPT"
	And User deletes the table "TC824_Table"
	
	
Scenario: TC847- Verify the field is not  displayed if allow to edit is set OFF for Singleline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC847_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC847_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Uncheck the "Allow to Edit" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC847_PPT | TC847_Table | 
	And User clicks on presentation "TC847_PPT"
	And Click on plus icon to add a record
	And Try to enter the data in "SinglelineCol" field
	Then User should not be able to enter data in "SinglelineCol" field
	And User deletes the presentation "TC847_PPT"
	And User deletes the table "TC847_Table"


Scenario: TC848- Verify the field is displayed if allow to edit is set ON for Singleline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC848_Table | SinglelineCol | Singleline |	
		
	When User navigate to forms and select the table "TC848_Table"
	And User go to the field settings tab
	And clicks on settings icon for "SinglelineCol" column
	And Checks whether "Allow to Edit" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC848_PPT | TC848_Table | 
	And User clicks on presentation "TC848_PPT"
	And Click on plus icon to add a record
	And Enter the data in "SinglelineCol" field as "DemoText"
	Then User should be able to enter data "DemoText" in "SinglelineCol" field 
	And User deletes the presentation "TC848_PPT"
	And User deletes the table "TC848_Table"
	
	
Scenario: TC849- Verify the field is displayed if allow to edit is set OFF for Multiline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC849_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC849_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Checks whether "Allow to Edit" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC849_PPT | TC849_Table | 
	And User clicks on presentation "TC849_PPT"
	And Click on plus icon to add a record
	And Try to enter in "MultilineCol" field
	Then User should not be able to enter data in "MultilineCol" field
	And User deletes the presentation "TC849_PPT"
	And User deletes the table "TC849_Table"
	
	
Scenario: TC850- Verify the field is displayed if allow to edit is set ON for Multiline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC850_Table | MultilineCol | Multiline |	
		
	When User navigate to forms and select the table "TC850_Table"
	And User go to the field settings tab
	And clicks on settings icon for "MultilineCol" column
	And Checks whether "Allow to Edit" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC850_PPT | TC850_Table | 
	And User clicks on presentation "TC850_PPT"
	And Click on plus icon to add a record
	And Enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	Then User should be able to enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And User deletes the presentation "TC850_PPT"
	And User deletes the table "TC850_Table"
	
	
Scenario: TC851- Verify the field is not  displayed if allow to edit is set OFF for Numeric field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC851_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC851_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Uncheck the "Allow to Edit" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC851_PPT | TC851_Table | 
	And User clicks on presentation "TC851_PPT"
	And Click on plus icon to add a record
	And Try to enter the data in "NumericCol" field
	Then User should not be able to enter data in "NumericCol" field
	And User deletes the presentation "TC851_PPT"
	And User deletes the table "TC851_Table"


Scenario: TC852- Verify the field is displayed if allow to edit is set ON for Numeric field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC852_Table | NumericCol | Numeric |	
		
	When User navigate to forms and select the table "TC852_Table"
	And User go to the field settings tab
	And clicks on settings icon for "NumericCol" column
	And Checks whether "Allow to Edit" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC852_PPT | TC852_Table | 
	And User clicks on presentation "TC852_PPT"
	And Click on plus icon to add a record
	And Enter the data in "NumericCol" field as "123"
	Then User should be able to enter data "123" in "NumericCol" field
	And User deletes the presentation "TC852_PPT"
	And User deletes the table "TC852_Table"
	
	
Scenario: TC853- Verify the field is displayed if allow to edit is set OFF for image cell field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC853_Table | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC853_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Uncheck the "Allow to View" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC853_PPT | TC853_Table | 
	And User clicks on presentation "TC853_PPT"
	And Click on plus icon to add a record
	And Try to upload in "AttachImage" field
	Then User should not be able to enter data in "AttachImage" field
	And User deletes the presentation "TC853_PPT"
	And User deletes the table "TC853_Table"
	
	
Scenario: TC854- Verify the field is not  displayed if allow to edit is set ON for image cell field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC854_Table | AttachImage | image cell |	
		
	When User navigate to forms and select the table "TC854_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachImage" column
	And Checks whether "Allow to Edit" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC854_PPT | TC854_Table | 
	And User clicks on presentation "TC854_PPT"
	And Click on plus icon to add a record
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG"
	Then User should be able to upload the attachment
	And User deletes the presentation "TC854_PPT"
	And User deletes the table "TC854_Table"
	
	
Scenario: TC855- Verify the field is displayed if allow to edit is set OFF for Atachment field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC855_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC855_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Uncheck the "Allow to Edit" option
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC855_PPT | TC855_Table | 
	And User clicks on presentation "TC855_PPT"
	And Click on plus icon to add a record
	And Try to upload in "AttachmentCol" field
	Then User should not be able to enter data in "AttachmentCol" field
	And User deletes the presentation "TC855_PPT"
	And User deletes the table "TC855_Table"
	
	
Scenario: TC856- Verify the field is not  displayed if allow to edit is set ON for Atachment field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC856_Table | AttachmentCol | Attachment |	
		
	When User navigate to forms and select the table "TC856_Table"
	And User go to the field settings tab
	And clicks on settings icon for "AttachmentCol" column
	And Checks whether "Allow to Edit" option is checked
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC856_PPT | TC856_Table | 
	And User clicks on presentation "TC856_PPT"
	And Click on plus icon to add a record
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Sample.txt"
	Then User should be able to upload the attachment
	And User deletes the presentation "TC856_PPT"
	And User deletes the table "TC856_Table"
	

Scenario: TC39 - To Verify whether user can select 'Approve' from 'Select' dropdown
	Given User create a Jiffy table with select field
		| tableName  |  columnName  | columnType | Option1 | Option2 | Option3 |
		| TC39_Table | SelectCol | Select |      Approve | Reject | Block |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC39_PPT | TC39_Table | 

	When User clicks on presentation "TC39_PPT"
	And Click on plus icon to add a record
	And Click on "SelectCol" field
	And Select the option "Approve"
	Then User should be able to select the "Approve" option
	And User deletes the presentation "TC39_PPT"
	And User deletes the table "TC39_Table"
	
	