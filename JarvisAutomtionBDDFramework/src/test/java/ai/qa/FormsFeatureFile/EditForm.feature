@P1_Smoke
Feature: Edit Forms
Scenario: TC387 - To verify whether user can Edit Singleline field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC387_Table | SinglelineCol | Singleline |
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC387_PPT | TC387_Table |
	When User clicks on presentation "TC387_PPT"
	And Click on plus icon
	And Enter the data in "SinglelineCol" field as "DemoText"
	And Save the record
	When User clicks on presentation "TC387_PPT" 
	And User clicks on edit record button
	And Clear the existing text in "SinglelineCol"
	And Enter the data in "SinglelineCol" field as "NewText"
	Then User should be able to edit the "SinglelineCol" field as "NewText"
	And User deletes the presentation "TC387_PPT"
	And User deletes the table "TC387_Table"

	
Scenario: TC389 - To verify whether user can Edit Numeric field
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC389_Table | NumericCol | Numeric |
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC389_PPT | TC389_Table |
	When User clicks on presentation "TC389_PPT"
	And Click on plus icon
	And Enter the data in "NumericCol" field as "123"
	And Save the record
	When User clicks on presentation "TC389_PPT" 
	And User clicks on edit record button
	And Clear the existing text in "NumericCol"
	And Enter the data in "NumericCol" field as "456"
	Then User should be able to edit the "NumericCol" field as "456"
	And User deletes the presentation "TC389_PPT"
	And User deletes the table "TC389_Table"
	
Scenario: TC7 - To verify whether user can edit the 'Createdby' field
	Given User create a Jiffy table with table name "TC7_Table"
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC7_PPT | TC7_Table | 

	When User clicks on presentation "TC7_PPT"
	And Click on plus icon
	And Try to enter the data in "CreatedBy" field
	Then User should not be able to enter data in "CreatedBy" field
	And User deletes the presentation "TC7_PPT"
	And User deletes the table "TC7_Table"
	
	
Scenario: TC9 - To verify whether user can edit the 'UpdatedBy' field 
	Given User create a Jiffy table with table name "TC9_Table"
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC9_PPT | TC9_Table | 

	When User clicks on presentation "TC9_PPT"
	And Click on plus icon
	And Try to enter the data in "UpdatedBy" field
	Then User should not be able to enter data in "UpdatedBy" field
	And User deletes the presentation "TC9_PPT"
	And User deletes the table "TC9_Table"
	
	
Scenario: TC11 - To Verify whether user can edit 'CreatedDate' field  
	Given User create a Jiffy table with table name "TC11_Table"
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC11_PPT | TC11_Table | 

	When User clicks on presentation "TC11_PPT"
	And Click on plus icon
	And Try to enter the data in "CreatedDate" field
	Then User should not be able to enter data in "CreatedDate" field
	And User deletes the presentation "TC11_PPT"
	And User deletes the table "TC11_Table"
	

Scenario: TC13 - To Verify whether user can edit 'UpdatedDate' field  
	Given User create a Jiffy table with table name "TC13_Table"	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC13_PPT | TC13_Table | 
	When User clicks on presentation "TC13_PPT"
	And Click on plus icon
	And Try to enter the data in "UpdatedDate" field
	Then User should not be able to enter data in "UpdatedDate" field
	And User deletes the presentation "TC13_PPT"
	And User deletes the table "TC13_Table"
	
	
Scenario: TC381 - To verify whether user can Edit UUID field 
	Given User create a Jiffy table with table name "TC381_Table"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC381_PPT | TC381_Table |
	When User clicks on presentation "TC381_PPT"
	And Click on plus icon
	And Save the record
	And User clicks on presentation "TC381_PPT" 
	And User clicks on edit record button
	And Try to enter the data in "UUID" field
	Then User should not be able to enter data in "UUID" field
	And User deletes the presentation "TC381_PPT"
	And User deletes the table "TC381_Table"
	
	
Scenario: TC382 - To verify whether user can Edit createby field  
	Given User create a Jiffy table with table name "TC382_Table"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC382_PPT | TC382_Table |
	When User clicks on presentation "TC382_PPT"
	And Click on plus icon
	And Save the record
	And User clicks on presentation "TC382_PPT" 
	And User clicks on edit record button
	And Try to enter the data in "CreatedBy" field
	Then User should not be able to enter data in "CreatedBy" field
	And User deletes the presentation "TC382_PPT"
	And User deletes the table "TC382_Table"
	
	
Scenario: TC383 - To verify whether user can Edit createdate field
	Given User create a Jiffy table with table name "TC383_Table"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC383_PPT | TC383_Table |
	When User clicks on presentation "TC383_PPT"
	And Click on plus icon
	And Save the record
	And User clicks on presentation "TC383_PPT" 
	And User clicks on edit record button
	And Try to enter the data in "CreatedDate" field
	Then User should not be able to enter data in "CreatedDate" field
	And User deletes the presentation "TC383_PPT"
	And User deletes the table "TC383_Table"
	
	
Scenario: TC384 - To verify whether user can Edit Updatedby field   
	Given User create a Jiffy table with table name "TC384_Table"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC384_PPT | TC384_Table |
	When User clicks on presentation "TC384_PPT"
	And Click on plus icon
	And Save the record
	And User clicks on presentation "TC384_PPT" 
	And User clicks on edit record button
	And Try to enter the data in "UpdatedBy" field
	Then User should not be able to enter data in "UpdatedBy" field
	And User deletes the presentation "TC384_PPT"
	And User deletes the table "TC384_Table"
	
	
Scenario: TC385 - To verify whether user can Edit Updateddate field 
	Given User create a Jiffy table with table name "TC385_Table"
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC385_PPT | TC385_Table |
	When User clicks on presentation "TC385_PPT"
	And Click on plus icon
	And Save the record
	And User clicks on presentation "TC385_PPT" 
	And User clicks on edit record button
	And Try to enter the data in "UpdatedDate" field
	Then User should not be able to enter data in "UpdatedDate" field
	And User deletes the presentation "TC385_PPT"
	And User deletes the table "TC385_Table"
	

Scenario: TC386 - To verify whether user can Edit execution state field 
	Given User create a Jiffy table with table name "TC386_Table"	
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC386_PPT | TC386_Table | 
	When User clicks on presentation "TC386_PPT"
	And Click on plus icon to add a record
	And Click on "Execution State" field
	And Select the state "New"
	And Save the record
	And User clicks on presentation "TC386_PPT" 
	And User clicks on edit record button
	And Click on "Execution State" field
	And Select the state "Completed"
	Then User should be able to select the "Completed" execution state
	And User deletes the presentation "TC386_PPT"
	And User deletes the table "TC386_Table"
	
		
Scenario: TC388 - To verify whether user can Edit Multiline field 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC388_Table | MultilineCol | Multiline |
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC388_PPT | TC388_Table |
	When User clicks on presentation "TC388_PPT"
	And Click on plus icon
	And Enter the data in "MultilineCol" field "Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo Demo "
	And Save the record
	And User clicks on presentation "TC388_PPT" 
	And User clicks on edit record button
	And Clear the text in "MultilineCol"
	And Enter the data in "MultilineCol" field "New New New New New New New New New New New New New New New New New New New New New New New "
	Then User should be able to enter the data in "MultilineCol" field "New New New New New New New New New New New New New New New New New New New New New New New "
	And User deletes the presentation "TC388_PPT"
	And User deletes the table "TC388_Table"
	
	
Scenario: TC391 - To verify whether user can add new image in imagefield    
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC391_Table | AttachImage | image cell |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC391_PPT | TC391_Table | 

	When User clicks on presentation "TC391_PPT"
	And Click on plus icon
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\Logo.PNG"
	And User clicks on presentation "TC391_PPT" 
	And User clicks on edit record button
	And Delete the existing image
	And Click on upload link to upload attachment "C:\Users\HP\Desktop\LoginBtn.PNG"
	Then User should be able to upload the attachment
	And User deletes the presentation "TC391_PPT"
	And User deletes the table "TC391_Table"
	
	
Scenario: TC394 - To verify whether user can edit URL field  
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC394_Table | URLCol | URL |	
		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC394_PPT | TC394_Table | 		
	When User clicks on presentation "TC394_PPT"
	And Click on plus icon
	And Enter the data in "URLCol" field as "https://www.google.com/"
	And Save the record
	And User clicks on presentation "TC394_PPT"
	And User clicks on edit record button
	And Clear the existing text in "URLCol"
	And Enter the data in "URLCol" field as "https://gmail.com/"
	Then User should be able to enter data "https://gmail.com/" in "URLCol" field
	And User deletes the presentation "TC394_PPT"
	And User deletes the table "TC394_Table"	
	
	
Scenario: TC396 - To verify whether user can edit Auto Generated field 
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC396_Table | AutoGenerateCol | Auto Generated |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC396_PPT | TC396_Table |
	When User clicks on presentation "TC396_PPT"
	And Click on plus icon
	And Save the record
	And User clicks on presentation "TC396_PPT"
	And User clicks on edit record button
	And Try to enter the data in "AutoGenerateCol" field
	Then User should not be able to enter data in "AutoGenerateCol" field
	And User deletes the presentation "TC396_PPT"
	And User deletes the table "TC396_Table"
	
	
Scenario: TC397- To verify whether user can edit 'Datetime' field   
	Given User create a Jiffy table with following details
		| tableName  |  columnName  | columnType |
		| TC397_Table | DateTimeCol | DateTime |
		
	And User creates the presentation 
		| presentationTitle | tableName | 
		| TC397_PPT | TC397_Table |
	When User clicks on presentation "TC397_PPT"
	And Click on plus icon
	And Enter the date in "DateTimeCol" field as "22"
	And Save the record
	And User clicks on presentation "TC397_PPT"
	And User clicks on edit record button
	And Enter the date in "DateTimeCol" field as "23"
	And Save the record
	Then User should be able to save the record
	And User deletes the presentation "TC397_PPT"
	And User deletes the table "TC397_Table"