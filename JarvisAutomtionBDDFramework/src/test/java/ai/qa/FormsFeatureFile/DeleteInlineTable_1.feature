@P1_Smoke
Feature: Delete Inline Table 1

Scenario: TC13 - Verify User should be able to delete record using 'DELETE RECORD' icon from inline table 1
Given User create a Jiffy table with inline table 1
		| tableName  |  inlineTableName |columnName  | columnType |
		| TC13_Table | TC13_InlineTable |SinglelineCol | Singleline |		
	And User creates the presentation 
		| presentationTitle |  tableName | 
		| TC13_PPT | TC13_Table | 		
	When User clicks on presentation "TC13_PPT"
	And Click on plus icon
	And Click on Add record link
	And Enter the data in inline table "SinglelineCol" field as "DemoText"
	And Save the record
	And Click on delete record icon
	Then  User should be able to delete record using 'DELETE RECORD' icon
	And Click on cancel
	And User deletes the presentation "TC13_PPT"
	And User deletes the table "TC13_Table"